#
# MoFEM is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# MoFEM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MoFEM. If not, see <http://www.gnu.org/licenses/>

include_directories(
  ${UM_SOURCE_DIR}/basic_finite_elements/
)

include_directories(
  ${UM_SOURCE_DIR}/${MODULE_NAME}/src
)

  add_executable(phase_field
    ${UM_SOURCE_DIR}/${MODULE_NAME}/phase_field.cpp
    ${UM_SOURCE_DIR}/${MODULE_NAME}/src/impl/PhaseField.cpp
  )
  target_link_libraries(phase_field
    users_modules
    mofem_finite_elements
    mofem_interfaces
    mofem_multi_indices
    mofem_petsc
    mofem_approx
    mofem_third_party
    mofem_cblas
    ${MoFEM_PROJECT_LIBS}
  )
  
file(COPY
  ${CMAKE_CURRENT_SOURCE_DIR}/meshes
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/
)
file(COPY
  ${CMAKE_CURRENT_SOURCE_DIR}/README.md
DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/)

file(COPY
  ${CMAKE_CURRENT_SOURCE_DIR}/do_vtk.sh
DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/)

  # cm_export_file("LShape.h5m" export_files_phase_field)

  #testing
  add_test(phase_field_1D_complex
    ${MoFEM_MPI_RUN} ${MPI_RUN_FLAGS} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/phase_field
     -my_file ${CMAKE_CURRENT_BINARY_DIR}/meshes/bar_1d_simpl.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps  
    -snes_type newtonls -snes_linesearch_type basic -snes_atol 1e-8 -snes_rtol 1e-8 -my_order 1 -snes_monitor -snes_max_it 10 
    -steps 100 -lambda 3 -gc 1. 
    -l0 0.25 -init_d 0. -eps_h 1e-8 -eps_k 1e-8 -my_output_prt 200 -calc_reactions 2 -steps_max_div 0 -is_atom_test 1 -p_func 1
  )
  #should converge in 6 iterations

  add_test(phase_field_3D_complex
    ${MoFEM_MPI_RUN} ${MPI_RUN_FLAGS} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/phase_field
    -my_file ${CMAKE_CURRENT_BINARY_DIR}/meshes/atom_test_3D.h5m -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps -steps_max_div 0
    -snes_type newtonls -snes_linesearch_type basic -snes_atol 1e-8 
    -snes_rtol 1e-8 -my_order 1 -snes_monitor -snes_max_it 10 -steps 10 -lambda 0.003 -gc 2.7e-3 -l0 0.5  
    -eps_h 1e-8 -eps_k 1e-8 -is_partitioned -calc_reactions 2 -my_output_prt 200 -is_atom_test 2 -p_func 1
  )
  add_test(phase_field_1D_FD
    ${MoFEM_MPI_RUN} ${MPI_RUN_FLAGS} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/phase_field
     -my_file ${CMAKE_CURRENT_BINARY_DIR}/meshes/bar_1d_simpl.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps  -steps_max_div 0
    -snes_type newtonls -snes_linesearch_type basic -snes_atol 1e-8 -snes_rtol 1e-8 -my_order 1 -snes_monitor -snes_max_it 10 
    -steps 100 -lambda 3 -gc 1. 
    -l0 0.25 -init_d 0. -eps_h 1e-8 -eps_k 1e-8 -my_output_prt 200 -calc_reactions 2 -is_atom_test 1 -p_func 2
  )
  #should converge in 6 iterations

  add_test(phase_field_3D_FD
    ${MoFEM_MPI_RUN} ${MPI_RUN_FLAGS} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/phase_field
    -my_file ${CMAKE_CURRENT_BINARY_DIR}/meshes/atom_test_3D.h5m -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps  
    -snes_type newtonls -snes_linesearch_type basic -snes_atol 1e-8 -steps_max_div 0
    -snes_rtol 1e-8 -my_order 1 -snes_monitor -snes_max_it 10 -steps 10 -lambda 0.003 -gc 2.7e-3 -l0 0.5 
    -eps_h 1e-8 -eps_k 1e-8 -is_partitioned -calc_reactions 2 -my_output_prt 200 -is_atom_test 2  -p_func 2
  )
  add_test(phase_field_1D_ANALITICAL
    ${MoFEM_MPI_RUN} ${MPI_RUN_FLAGS} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/phase_field
     -my_file ${CMAKE_CURRENT_BINARY_DIR}/meshes/bar_1d_simpl.cub -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps  -steps_max_div 0
    -snes_type newtonls -snes_linesearch_type basic -snes_atol 1e-8 -snes_rtol 1e-8 -my_order 1 -snes_monitor -snes_max_it 10 
    -steps 100 -lambda 3 -gc 1. 
    -l0 0.25 -init_d 0. -eps_h 1e-8 -eps_k 1e-8 -my_output_prt 200 -calc_reactions 2 -is_atom_test 1 -p_func 3
  )

  add_test(phase_field_3D_ANALITICAL
  ${MoFEM_MPI_RUN} ${MPI_RUN_FLAGS} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/phase_field
  -my_file ${CMAKE_CURRENT_BINARY_DIR}/meshes/atom_test_3D.h5m -ksp_type fgmres -pc_type lu -pc_factor_mat_solver_package mumps  
  -snes_type newtonls -snes_linesearch_type basic -snes_atol 1e-8 -steps_max_div 0
  -snes_rtol 1e-8 -my_order 1 -snes_monitor -snes_max_it 10 -steps 10 -lambda 0.003 -gc 2.7e-3 -l0 0.5 
  -eps_h 1e-8 -eps_k 1e-8 -is_partitioned -calc_reactions 2 -my_output_prt 200 -is_atom_test 2  -p_func 3
)

