Phase field fracture  
<!-- {#um_phase_field_readnme} -->
========================

This module allows for computational simulation of brittle fracture by means of a higher order finite element approach based on phase-field.
References: 

1. [Phase-field models for brittle and cohesive fracture](https://link.springer.com/article/10.1007/s11012-013-9862-0)
2. [A phase field model for rate-independent crack propagation: Robust algorithmic implementation based on operator splits](https://www.sciencedirect.com/science/article/pii/S0045782510001283)
3. [Phase field modelling of fracture](https://www.researchgate.net/profile/Jian_Ying_Wu/publication/326258921_Phase_field_modelling_of_fracture/links/5b41dc44458515f71cb17cca/Phase-field-modelling-of-fracture.pdf)
4. [Abaqus implementation of monolithic and staggered schemes for quasi-static and dynamic fracture phase-field model](https://www.sciencedirect.com/science/article/pii/S0927025616301550#b0195)
5. [Using Complex Variables to Estimate Derivatives of Real Functions](https://epubs.siam.org/doi/abs/10.1137/S003614459631241X)

For more instructions, tutorials visit [MoFEM page](http://mofem.eng.gla.ac.uk/mofem/html/pages.html).

![-](doc/phase_field_3l.gif)

Check before run
================

* Check where is your mesh file
* Check material parameters
* Check if mesh is partitioned


Phase-field brittle fracture, notched plate
=========================
First, partition the mesh:
(skip this step if only one processor is used)

~~~~~
../tools/mofem_part -my_file ./meshes/notched_plate.cub -my_nparts 6 
mv out.h5m notched_plate.h5m
~~~~~

Next, we start analysis on a partitioned (6 cores) output mesh file with the following:

~~~~~
mpirun -np 6 ./phase_field -my_file notched_plate.h5m -ksp_type fgmres -pc_type lu \
-pc_factor_mat_solver_package mumps -snes_type newtonls -snes_linesearch_type basic \
-snes_atol 1e-8 -snes_rtol 1e-8 -my_order 1 -snes_monitor -snes_max_it 50 -snes_max_it 15 \
-steps 1000 -lambda 0.1 -gc 2.7e-3 -l0 0.02  \
-my_order 1 -eps_h 1e-8 -eps_k 1e-6 \
-calc_reactions 2 -my_output_prt 1 \
-is_partitioned | tee log

~~~~~

Module parameters:

~~~~~
  -my_order
  -my_output_prt
  -steps
  -steps_max_div
  -lambda
  -step_red
  -init_d
  -is_atom_test
  -calc_reactions
  -is_partitioned
  -elastic_only
  -p_func
  -gc
  -l0
  -eps_h
  -eps_k
~~~~~


Convert output files (for [Paraview](https://www.paraview.org/))
=========================

~~~~~
./do_vtk.sh
~~~~~
Produce reaction, lambda graphs in gluplot
=========================

~~~~~
grep "xyz" log | awk '{print $2,$5,$9,$10,$11}' | tee log_upd


gnuplot
plot "log_upd" using 1:2 title 'force' with linespoints
~~~~~





