/** \file phase_field.cpp
 * */
/* MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>.
 *
 * */
#include <BasicFiniteElements.hpp>
#include <PhaseField.hpp>

using namespace MoFEM;

static char help[] = "Arguments specific for phase-field module. \n"
                     "\n";

int main(int argc, char *argv[]) {

  MoFEM::Core::Initialize(&argc, &argv, (char *)0, help);
  // Create mesh database
  moab::Core mb_instance;              // create database
  moab::Interface &moab = mb_instance; // create interface to database

  MPI_Comm moab_comm_world;
  MPI_Comm_dup(PETSC_COMM_WORLD, &moab_comm_world);
  ParallelComm *pcomm = ParallelComm::get_pcomm(&moab, MYPCOMM_INDEX);
  if (pcomm == NULL)
    pcomm = new ParallelComm(&moab, moab_comm_world);

  try {
    char mesh_file_name[255];
    PetscBool is_partitioned = PETSC_FALSE;
    PetscBool flg_file = PETSC_FALSE;

    CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "Phase field mesh:", "none");
    CHKERR PetscOptionsString("-my_file", "mesh file name", "", "mesh.h5m",
                              mesh_file_name, 255, &flg_file);
    CHKERR PetscOptionsBool("-is_partitioned", "is_partitioned?", "",
                            is_partitioned, &is_partitioned, PETSC_NULL);

    ierr = PetscOptionsEnd();
    CHKERRQ(ierr);

    if (flg_file != PETSC_TRUE) {
      SETERRQ(PETSC_COMM_SELF, 1, "*** ERROR -my_file (MESH FILE NEEDED)");
    }
    // Create mesh database
    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    // Create moab communicator
    MPI_Comm moab_comm_world;
    MPI_Comm_dup(PETSC_COMM_WORLD, &moab_comm_world);
    ParallelComm *pcomm = ParallelComm::get_pcomm(&moab, MYPCOMM_INDEX);
    if (pcomm == NULL)
      pcomm = new ParallelComm(&moab, moab_comm_world);

    // Read whole mesh or part of is if partitioned
    if (is_partitioned == PETSC_TRUE) {
      const char *option;
      option = "PARALLEL=READ_PART;"
               "PARALLEL_RESOLVE_SHARED_ENTS;"
               "PARTITION=PARALLEL_PARTITION;";
      CHKERR moab.load_file(mesh_file_name, 0, option);
    } else {
      const char *option;
      option = "";
      CHKERR moab.load_file(mesh_file_name, 0, option);
    }

    MoFEM::Core core(moab);
    MoFEM::Interface &m_field = core;

    PhaseField::CommonData commonData(m_field);
    commonData.getParameters();

    PhaseField phase_field(m_field, commonData);

    CHKERR phase_field.getParameters();
    CHKERR phase_field.addFields();
    CHKERR phase_field.addMomentumFluxes();
    CHKERR phase_field.addElements();
    CHKERR phase_field.setOperators();
    CHKERR phase_field.buildDM();
    CHKERR phase_field.setupProblem();
    CHKERR phase_field.setupBoundaryConditions();
    CHKERR phase_field.setupSolver();
    CHKERR phase_field.solveDM();
  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}