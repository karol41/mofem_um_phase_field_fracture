#include <BasicFiniteElements.hpp>
#include <PhaseField.hpp>

double PhaseField::LoadScale::lambda = 1;

MoFEMErrorCode PhaseField::getParameters() {
  MoFEMFunctionBeginHot;
  oRder = 1;              // default approximation order
  nbSubSteps = 10;        // default number of steps
  lAmbda0 = 0.1;          // default step size
  initD = 0.0;            // initial phase_field value
  stepRed = 0.2;          // stepsize reduction while diverging to 20%
  maxDivStep = 100;       // maximumim number of diverged steps
  outPutStep = 1;         // how often post processing data is saved to h5m file
  reactionsSidesetId = 0; // id of the sideset for calculating reactions
  isAtomTest = PETSC_FALSE;
  testNb = 1;
  calcReactions = PETSC_FALSE;
  elasticOnly = PETSC_FALSE;
  isPartitioned = PETSC_FALSE;

  // Print boundary conditions and material parameters
  MeshsetsManager *meshsets_mng_ptr;
  CHKERR mField.getInterface(meshsets_mng_ptr);
  CHKERR meshsets_mng_ptr->printDisplacementSet();
  CHKERR meshsets_mng_ptr->printForceSet();
  CHKERR meshsets_mng_ptr->printMaterialsSet();

  CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "",
                           "Phase field module options:", "none");
  // Set approximation order
  CHKERR PetscOptionsInt("-my_order", "approximation order", "", oRder, &oRder,
                         PETSC_NULL);

  CHKERR PetscOptionsInt("-my_output_prt",
                         "frequncy how often results are dumped on hard disk",
                         "", 1, &outPutStep, NULL);

  CHKERR PetscOptionsInt("-steps", "number of steps", "", nbSubSteps,
                         &nbSubSteps, PETSC_NULL);

  CHKERR PetscOptionsInt("-steps_max_div", "number of steps", "", maxDivStep,
                         &maxDivStep, PETSC_NULL);

  CHKERR PetscOptionsScalar("-lambda", "lambda", "", lAmbda0, &lAmbda0,
                            PETSC_NULL);
  CHKERR PetscOptionsScalar("-step_red", "step reductcion while diverge", "", stepRed, &stepRed,
                            PETSC_NULL);
  CHKERR PetscOptionsScalar("-init_d", "initial phasefield", "", initD, &initD, PETSC_NULL);

  CHKERR PetscOptionsInt("-is_atom_test", "Give number of the atom test", "",
                         testNb, &testNb, &isAtomTest);
  CHKERR PetscOptionsInt("-calc_reactions", "calculate reactions for blocksets",
                         "", reactionsSidesetId, &reactionsSidesetId,
                         &calcReactions);
  CHKERR PetscOptionsBool("-is_partitioned", "is_partitioned?", "",
                          isPartitioned, &isPartitioned, PETSC_NULL);

  CHKERR PetscOptionsBool("-elastic_only", "calculate without phase field", "",
                          elasticOnly, &elasticOnly, PETSC_NULL);

  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);

  MoFEMFunctionReturnHot(0);
}

MoFEMErrorCode PhaseField::addFields() {
  MoFEMFunctionBegin;

  bitLevel.set(0);

  CHKERR mField.getInterface<BitRefManager>()->setBitRefLevelByDim(0, 3,
                                                                   bitLevel);

  CHKERR mField.add_field("MESH_NODE_POSITIONS", H1, AINSWORTH_LEGENDRE_BASE,
                          3);
  CHKERR mField.add_ents_to_field_by_type(0, MBTET, "MESH_NODE_POSITIONS");
  CHKERR mField.add_field("U", H1, AINSWORTH_LEGENDRE_BASE, 3);

  CHKERR mField.add_ents_to_field_by_type(0, MBTET, "U");

  CHKERR mField.set_field_order(0, MBVERTEX, "U", 1);
  CHKERR mField.set_field_order(0, MBEDGE, "U", oRder);
  CHKERR mField.set_field_order(0, MBTRI, "U", oRder);
  CHKERR mField.set_field_order(0, MBTET, "U", oRder);
  CHKERR mField.set_field_order(0, MBVERTEX, "MESH_NODE_POSITIONS", 1);

  CHKERR mField.add_field("PHASE_FIELD", H1, AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR mField.add_ents_to_field_by_type(0, MBTET, "PHASE_FIELD");
  CHKERR mField.set_field_order(0, MBVERTEX, "PHASE_FIELD", 1);
  CHKERR mField.set_field_order(0, MBEDGE, "PHASE_FIELD", oRder);
  CHKERR mField.set_field_order(0, MBTRI, "PHASE_FIELD", oRder);
  CHKERR mField.set_field_order(0, MBTET, "PHASE_FIELD", oRder);

  CHKERR mField.build_fields();

  {
    Projection10NodeCoordsOnField ent_method_material(mField,
                                                      "MESH_NODE_POSITIONS");
    CHKERR mField.loop_dofs("MESH_NODE_POSITIONS", ent_method_material);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode PhaseField::addElements() {
  MoFEMFunctionBegin;

  // Add finite element (this defines element declaration comes later)
  CHKERR mField.add_finite_element("ELASTIC");
  CHKERR mField.modify_finite_element_add_field_row("ELASTIC", "U");
  CHKERR mField.modify_finite_element_add_field_col("ELASTIC", "U");
  CHKERR mField.modify_finite_element_add_field_data("ELASTIC", "U");
  if (!elasticOnly) {
    CHKERR mField.modify_finite_element_add_field_row("ELASTIC", "PHASE_FIELD");
    CHKERR mField.modify_finite_element_add_field_col("ELASTIC", "PHASE_FIELD");
  }
  CHKERR mField.modify_finite_element_add_field_data("ELASTIC", "PHASE_FIELD");

  CHKERR mField.modify_finite_element_add_field_data("ELASTIC",
                                                     "MESH_NODE_POSITIONS");
  // Add entities to that element
  CHKERR mField.add_ents_to_finite_element_by_type(0, MBTET, "ELASTIC");
  // build finite elements
  CHKERR mField.build_finite_elements();
  // build adjacencies between elements and degrees of freedom
  CHKERR mField.build_adjacencies(bitLevel);

  CHKERR mField.getInterface<FieldBlas>()->setField(initD, MBVERTEX,
                                                    "PHASE_FIELD");
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode PhaseField::addMomentumFluxes() {

  MoFEMFunctionBegin;

  CHKERR MetaNeummanForces::addNeumannBCElements(mField, "U");
  CHKERR MetaNodalForces::addElement(mField, "U");
  CHKERR MetaEdgeForces::addElement(mField, "U");

  MoFEMFunctionReturn(0);
}
MoFEMErrorCode PhaseField::buildDM() {
  MoFEMFunctionBegin;
  dmName = "DM_ELASTIC";

  // Register DM problem
  CHKERR DMRegister_MoFEM(dmName);
  CHKERR DMCreate(PETSC_COMM_WORLD, &dm);
  CHKERR DMSetType(dm, dmName);
  // Create DM instance
  CHKERR DMMoFEMCreateMoFEM(dm, &mField, dmName, bitLevel);
  // Configure DM form line command options (DM itself, solvers,
  // pre-conditioners, ... )
  CHKERR DMSetFromOptions(dm);
  // Add elements to dm (only one here)
  CHKERR DMMoFEMAddElement(dm, "ELASTIC");
  if (mField.check_finite_element("PRESSURE_FE"))
    CHKERR DMMoFEMAddElement(dm, "PRESSURE_FE");
  if (mField.check_finite_element("FORCE_FE"))
    CHKERR DMMoFEMAddElement(dm, "FORCE_FE");
  CHKERR DMMoFEMSetIsPartitioned(dm, isPartitioned);
  // setup the DM
  CHKERR DMSetUp(dm);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode PhaseField::setOperators() {
  MoFEMFunctionBegin;

  boost::shared_ptr<FEMethod> nullFE;
  feLhs = boost::make_shared<VolumeElementForcesAndSourcesCore>(mField);

  feLhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("PHASE_FIELD", commonData.cPtr));
  feLhs->getOpPtrVector().push_back(new OpCalculateScalarFieldGradient<3>(
      "PHASE_FIELD", commonData.gradcPtr));
  feLhs->getOpPtrVector().push_back(
      new OpCalculateVectorFieldGradient<3, 3>("U", commonData.gradDispPtr));

  // loop over blocks
  for (auto &sit : commonData.setOfBlocksData) {

    feLhs->getOpPtrVector().push_back(
        new OpCalculateStress(mField, "U", commonData, sit.second));
    feLhs->getOpPtrVector().push_back(
        new OpAssembleULhs_dU(commonData, sit.second));
    feLhs->getOpPtrVector().push_back(
        new OpAssembleULhs_dd(commonData, sit.second));
    feLhs->getOpPtrVector().push_back(
        new OpAssembleDLhs_dU(commonData, sit.second));
    feLhs->getOpPtrVector().push_back(
        new OpAssembleDLhs_dd(commonData, sit.second));
  }

  feRhs = boost::make_shared<VolumeElementForcesAndSourcesCore>(mField);

  feRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("PHASE_FIELD", commonData.cPtr));
  feRhs->getOpPtrVector().push_back(new OpCalculateScalarFieldGradient<3>(
      "PHASE_FIELD", commonData.gradcPtr));
  feRhs->getOpPtrVector().push_back(
      new OpCalculateVectorFieldGradient<3, 3>("U", commonData.gradDispPtr));

  // loop over blocks
  for (auto &sit : commonData.setOfBlocksData) {

    feRhs->getOpPtrVector().push_back(
        new OpCalculateStress(mField, "U", commonData, sit.second));
    feRhs->getOpPtrVector().push_back(
        new OpAssembleURhs(commonData, sit.second));
    feRhs->getOpPtrVector().push_back(
        new OpAssembleDRhs(commonData, sit.second));
  }

  feUpdate = boost::make_shared<VolumeElementForcesAndSourcesCore>(mField);

  feUpdate->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("PHASE_FIELD", commonData.cPtr));
  feUpdate->getOpPtrVector().push_back(
      new OpCalculateVectorFieldGradient<3, 3>("U", commonData.gradDispPtr));

  for (auto &sit : commonData.setOfBlocksData) {
    feUpdate->getOpPtrVector().push_back(
        new OpUpdateHistory(mField, "U", commonData, sit.second));
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode PhaseField::setupProblem() {
  MoFEMFunctionBegin;

  CHKERR DMCreateGlobalVector_MoFEM(dm, &D);
  CHKERR VecDuplicate(D, &F_res);
  CHKERR VecDuplicate(D, &D0);
  CHKERR DMCreateMatrix_MoFEM(dm, &Aij);
  CHKERR DMoFEMMeshToLocalVector(dm, D, INSERT_VALUES, SCATTER_FORWARD);
  CHKERR VecGhostUpdateBegin(D, INSERT_VALUES, SCATTER_FORWARD);
  CHKERR VecGhostUpdateEnd(D, INSERT_VALUES, SCATTER_FORWARD);
  CHKERR MatZeroEntries(Aij);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode PhaseField::setupBoundaryConditions() {
  MoFEMFunctionBegin;

  dirichletBcPtr =
      boost::make_shared<DirichletDisplacementBc>(mField, "U", Aij, D, F_res);
  dirichletBcPtr->methodsOp.push_back(new LoadScale());

  // Assemble pressure and traction forces. Run particular implemented for do
  // this, see
  // MetaNeummanForces how this is implemented.
  CHKERR MetaNeummanForces::setMomentumFluxOperators(mField, neumannForces,
                                                     NULL, "U");
  {
    auto fit = neumannForces.begin();
    for (; fit != neumannForces.end(); fit++) {
      fit->second->methodsOp.push_back(new LoadScale());
    }
  }

  // // Assemble forces applied to nodes, see implementation in NodalForce
  CHKERR MetaNodalForces::setOperators(mField, nodalForces, NULL, "U");
  {
    auto fit = nodalForces.begin();
    for (; fit != nodalForces.end(); fit++) {
      fit->second->methodsOp.push_back(new LoadScale());
    }
  }

  // // Assemble edge forces
  CHKERR MetaEdgeForces::setOperators(mField, edgeForces, NULL, "U");
  {
    auto fit = edgeForces.begin();
    for (; fit != edgeForces.end(); fit++) {
      fit->second->methodsOp.push_back(new LoadScale());
    }
  }

  boost::shared_ptr<FEMethod> fe_null;
  // Rhs
  CHKERR DMMoFEMSNESSetFunction(dm, DM_NO_ELEMENT, fe_null, dirichletBcPtr,
                                fe_null); // TODO: check if necessary

  {
    auto fit = neumannForces.begin();
    for (; fit != neumannForces.end(); fit++) {
      CHKERR DMMoFEMSNESSetFunction(dm, fit->first.c_str(),
                                    &fit->second->getLoopFe(), NULL, NULL);
    }
  }
  {
    auto fit = nodalForces.begin();
    for (; fit != nodalForces.end(); fit++) {
      CHKERR DMMoFEMSNESSetFunction(dm, fit->first.c_str(),
                                    &fit->second->getLoopFe(), NULL, NULL);
    }
  }
  {
    auto fit = edgeForces.begin();
    for (; fit != edgeForces.end(); fit++) {
      CHKERR DMMoFEMSNESSetFunction(dm, fit->first.c_str(),
                                    &fit->second->getLoopFe(), NULL, NULL);
    }
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode PhaseField::setupSolver() {
  MoFEMFunctionBegin;

  boost::shared_ptr<FEMethod> fe_null;

  // Rhs
  CHKERR DMMoFEMSNESSetFunction(dm, "ELASTIC", feRhs, fe_null, fe_null);
  CHKERR DMMoFEMSNESSetFunction(dm, DM_NO_ELEMENT, fe_null, fe_null,
                                dirichletBcPtr);
  // Lhs
  CHKERR DMMoFEMSNESSetJacobian(dm, DM_NO_ELEMENT, fe_null, dirichletBcPtr,
                                fe_null);
  CHKERR DMMoFEMSNESSetJacobian(dm, "ELASTIC", feLhs, fe_null, fe_null);
  CHKERR DMMoFEMSNESSetJacobian(dm, DM_NO_ELEMENT, fe_null, fe_null,
                                dirichletBcPtr);

  SnesCtx *snes_ctx;
  // create snes nonlinear solver
  {
    CHKERR SNESCreate(PETSC_COMM_WORLD, &snes);
    CHKERR DMMoFEMGetSnesCtx(dm, &snes_ctx);
    CHKERR SNESSetFunction(snes, F_res, SnesRhs, snes_ctx);
    CHKERR SNESSetJacobian(snes, Aij, Aij, SnesMat, snes_ctx);
    CHKERR SNESSetFromOptions(snes);
  }

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode PhaseField::solveDM() {

  MoFEMFunctionBegin;

  SNESConvergedReason snes_reason;
  int number_of_diverges = 0;
  int desired_iteration_number = 5; // TODO: improve adaptivity
  double step_size = lAmbda0 / nbSubSteps;
  LoadScale::lambda = 0;
  VectorDouble reactions(3);
  for (int ss = 0; ss < nbSubSteps; ++ss) {
    LoadScale::lambda += step_size;
    // for testing loading //TODO: Implement load history config file
    // if(ss < nbSubSteps/3)
    //   LoadScale::lambda += lambda0 / nbSubSteps;
    // else
    //   LoadScale::lambda -= lambda0 / nbSubSteps;

    CHKERR VecCopy(D, D0);
    CHKERR VecGhostUpdateBegin(D0, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateEnd(D0, INSERT_VALUES, SCATTER_FORWARD);

    dirichletBcPtr->snes_ctx = FEMethod::CTX_SNESNONE;
    dirichletBcPtr->snes_x = D;
    CHKERR DMoFEMPreProcessFiniteElements(dm, dirichletBcPtr.get());
    CHKERR VecAssemblyBegin(D);
    CHKERR VecAssemblyEnd(D);
    CHKERR SNESSolve(snes, PETSC_NULL, D);

    CHKERR SNESGetConvergedReason(snes, &snes_reason);
    int its;
    CHKERR SNESGetIterationNumber(snes, &its);
    CHKERR PetscPrintf(PETSC_COMM_WORLD,
                       "%s Number of nonlinear iterations = %D\n",
                       SNESConvergedReasons[snes_reason], its);
    // adaptivity

    if (snes_reason < 0) {
      CHKERR PetscPrintf(PETSC_COMM_WORLD, "Nonlinear solver diverged!\n");
      if (isAtomTest)
        SETERRQ(PETSC_COMM_SELF, MOFEM_ATOM_TEST_INVALID,
                "atom test diverged!");
      if (number_of_diverges < maxDivStep) {
        CHKERR VecCopy(D0, D);
        LoadScale::lambda -= 2 * step_size;
        LoadScale::lambda += stepRed * (step_size);
        CHKERR PetscPrintf(PETSC_COMM_WORLD, "Reducing step... \n");
        number_of_diverges++;
        ss--;
        continue;

      } else {
        break;
      }
    }
    // ADAPTIVE STEPPING
    // const double frac = (double)desired_iteration_number / its;
    // step_size *= sqrt(frac);

    CHKERR VecGhostUpdateBegin(D, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateEnd(D, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR DMoFEMMeshToGlobalVector(dm, D, INSERT_VALUES, SCATTER_REVERSE);

    CHKERR VecAYPX(D0, -1., D);
    CHKERR VecGhostUpdateBegin(D0, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateEnd(D0, INSERT_VALUES, SCATTER_FORWARD);

    CHKERR DMoFEMLoopFiniteElements(dm, "ELASTIC", feUpdate);
    if (ss % outPutStep == 0) {
      // for postprocessing:
      if (!postProcPtr) {
        postProcPtr = boost::make_shared<PostProcVolumeOnRefinedMesh>(mField);
        CHKERR postProcPtr->generateReferenceElementMesh();
        CHKERR postProcPtr->addFieldValuesPostProc("U");
        CHKERR postProcPtr->addFieldValuesPostProc("PHASE_FIELD");
        CHKERR postProcPtr->addFieldValuesPostProc("PHASE_FIELD",
                                                   "PHASE_FIELD_INC", D0);
        postProcPtr->getOpPtrVector().push_back(
            new OpCalculateScalarFieldValues("PHASE_FIELD", commonData.cPtr));
        postProcPtr->getOpPtrVector().push_back(
            new OpCalculateVectorFieldGradient<3, 3>("U",
                                                     commonData.gradDispPtr));
        for (auto &sit : commonData.setOfBlocksData) {
          postProcPtr->getOpPtrVector().push_back(new OpPostProcStress(
              postProcPtr->postProcMesh, postProcPtr->mapGaussPts, commonData,
              sit.second));
        }
      }
      CHKERR DMoFEMLoopFiniteElements(dm, "ELASTIC", postProcPtr);
      string out_file_name;
      std::ostringstream stm;
      stm << "out_" << ss << ".h5m";
      out_file_name = stm.str();
      CHKERR PetscPrintf(PETSC_COMM_WORLD, "out file %s\n",
                         out_file_name.c_str());
      CHKERR postProcPtr->postProcMesh.write_file(out_file_name.c_str(), "MOAB",
                                                  "PARALLEL=WRITE_PART");
    }

    if (calcReactions) {
      CHKERR calcReactionForces(reactions);
      CHKERR PetscPrintf(
          PETSC_COMM_WORLD,
          "Step: %d | Lambda: %6.4e | Reactions xyz:  %6.4e  %6.4e  %6.4e \n",
          ss, LoadScale::lambda, reactions[0], reactions[1], reactions[2]);

    } else {
      CHKERR PetscPrintf(PETSC_COMM_WORLD, "Step: %d | Lambda: %6.4e  \n", ss,
                         LoadScale::lambda);
    }
    // ### ATOM TESTS
    if (isAtomTest)
      CHKERR atomTests(ss, reactions);
  }

  CHKERR SNESDestroy(&snes);
  CHKERR MatDestroy(&Aij);
  CHKERR VecDestroy(&D);
  CHKERR VecDestroy(&D0);
  CHKERR VecDestroy(&F_res);
  CHKERR DMDestroy(&dm);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode PhaseField::calcReactionForces(VectorDouble &reactions) {
  MoFEMFunctionBegin;

  Vec F_int;
  CHKERR DMCreateGlobalVector_MoFEM(dm, &F_int);

  feRhs->snes_ctx = FEMethod::CTX_SNESSETFUNCTION;
  feRhs->snes_f = F_int;
  CHKERR DMoFEMLoopFiniteElements(dm, "ELASTIC", feRhs);

  CHKERR VecAssemblyBegin(F_int);
  CHKERR VecAssemblyEnd(F_int);
  CHKERR VecGhostUpdateBegin(F_int, INSERT_VALUES, SCATTER_FORWARD);
  CHKERR VecGhostUpdateEnd(F_int, INSERT_VALUES, SCATTER_FORWARD);

  Reactions my_react(mField, "DM_ELASTIC", "U");
  CHKERR my_react.calculateReactions(F_int);

  const auto &reactions_map = my_react.getReactionsMap();
  const int id = reactionsSidesetId;
  reactions.clear();
  const bool check_key = reactions_map.find(id) == reactions_map.end();
  if (!check_key) {
    reactions = reactions_map.at(id);
  }
  Vec react_vec;
  CHKERR VecCreateMPI(mField.get_comm(), 1, PETSC_DECIDE, &react_vec);
  for (int dir : {0, 1, 2}) {
    CHKERR VecZeroEntries(react_vec);
    const int idx = mField.get_comm_rank();
    CHKERR VecSetValues(react_vec, 1, &idx, &reactions[dir], INSERT_VALUES);
    CHKERR VecAssemblyBegin(react_vec);
    CHKERR VecAssemblyEnd(react_vec);
    double sum = 0;
    CHKERR VecSum(react_vec, &sum);
    reactions[dir] = sum;
  }
  CHKERR VecDestroy(&react_vec);
  CHKERR VecDestroy(&F_int);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode PhaseField::atomTests(const int step_number,
                                     const VectorDouble &reactions) {

  MoFEMFunctionBegin;

  if (testNb == 1) {
    if (step_number == 76) {
      auto phase_field_dof =
          mField.get_dofs_by_name_and_type_begin("PHASE_FIELD", MBVERTEX);
      const auto phase_field_check = (*phase_field_dof)->getFieldData();
      const double strain = (*commonData.sTrainPtr)(
          5, 0); // e[3,3] of first gauss point //symmetric
      const double phase_field_err = fabs(phase_field_check - 0.25);
      const double strain_error =
          fabs(sqrt(commonData.gC / (3 * commonData.yOung * commonData.l0)) -
               strain); // analitical solution for 1D case
      if (strain_error > 1e-3 || phase_field_err > 1e-3) {
        CHKERR PetscPrintf(PETSC_COMM_WORLD,
                           "Strain error: %g. Phase-field error: %g \n",
                           strain_error, phase_field_err);
        SETERRQ(PETSC_COMM_SELF, MOFEM_ATOM_TEST_INVALID,
                "atom test diverged!");
      }
    }
  } else if (testNb == 2) {
    if (step_number == 9) {
      if (fabs(reactions(1) + 2.0726e-01) > 1e-3)
        SETERRQ(PETSC_COMM_SELF, MOFEM_ATOM_TEST_INVALID, "atom test diverged");
    }
  }

  MoFEMFunctionReturn(0);
}

template <class T1, class T2>
MoFEMErrorCode PhaseField::CommonData::strainSpecDecomp(const T1 &strain,
                                                        T2 &positive_strain,
                                                        T2 &negative_strain) {
  MoFEMFunctionBeginHot;
  FTensor::Tensor1<double, 3> eigen_values;
  FTensor::Tensor2<double, 3, 3>
      eigen_vectors; 

  T2 Aplus(0., 0., 0., 0., 0., 0.);
  T2 Aminus(0., 0., 0., 0., 0., 0.);
  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;
  FTensor::Index<'k', 3> k;
  FTensor::Index<'l', 3> l;

  auto bracket_plus = [](auto &a) { return a > 0. ? a : 0; };
  auto bracket_minus = [](auto &a) { return a < 0. ? a : 0; };

  symm_to_non_tensor(strain, eigen_vectors);

  // LAPACK - eigenvalues and vectors. Applied twice for initial creates
  // memory space
  int n = 3, lda = 3, info, lwork = -1;
  double wkopt;
  info = lapack_dsyev('V', 'U', n, &(eigen_vectors(0, 0)), lda,
                      &(eigen_values(0)), &wkopt, lwork);
  if (info != 0)
    SETERRQ1(PETSC_COMM_SELF, 1,
             "something is wrong with lapack_dsyev info = %d", info);

  // for complex
  // info  = lapack_zheev('V', 'U', n, &(eigen_vectors(0, 0)), lda,
  //                     &(eigen_values(0)), work, lwork);
  lwork = (int)wkopt;
  double work[lwork];
  info = lapack_dsyev('V', 'U', n, &(eigen_vectors(0, 0)), lda,
                      &(eigen_values(0)), work, lwork);
  if (info != 0)
    SETERRQ1(PETSC_COMM_SELF, 1,
             "something is wrong with lapack_dsyev info = %d", info);

  for (int ii : {0, 1, 2}) {
    Aplus(ii, ii) = bracket_plus(eigen_values(ii));
    Aminus(ii, ii) = bracket_minus(eigen_values(ii));
  }
  positive_strain(i, j) =
      eigen_vectors(l, i) ^ (Aplus(l, k) * eigen_vectors(k, j));
  negative_strain(i, j) =
      eigen_vectors(l, i) ^ (Aminus(l, k) * eigen_vectors(k, j)); 

  MoFEMFunctionReturnHot(0);
}

MoFEMErrorCode PhaseField::CommonData::calculateStrainPlusComplex(
    const FTensor::Tensor2<Cplx, 3, 3> &strain,
    FTensor::Tensor2<Cplx, 3, 3> &strainP) {
  MoFEMFunctionBeginHot;

  FTensor::Tensor1<Cplx, 3> eigen_values;
  FTensor::Tensor2<Cplx, 3, 3> eigen_vectors;
  FTensor::Tensor2<Cplx, 3, 3> eigen_vectors_right;
  FTensor::Tensor2<Cplx, 3, 3> Aplus(0., 0., 0., 0., 0., 0., 0., 0., 0.);
  FTensor::Tensor2<Cplx, 3, 3> positive_strain;
  const double mu = this->mU;
  const double lambda = this->lAmbda;
  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;
  FTensor::Index<'k', 3> k;
  FTensor::Index<'l', 3> l;
  auto bracket_plus = [](auto a) { // IMPORTANT
    return 0.5 * (a + sqrt(a * a));
  };

  auto inverse = [](const FTensor::Tensor2<Cplx, 3, 3> &a) {
    FTensor::Tensor2<Cplx, 3, 3> inv_a;
    Cplx det = a(0, 0) * a(1, 1) * a(2, 2) + a(1, 0) * a(2, 1) * a(0, 2) +
               a(2, 0) * a(0, 1) * a(1, 2) - a(0, 0) * a(2, 1) * a(1, 2) -
               a(2, 0) * a(1, 1) * a(0, 2) - a(1, 0) * a(0, 1) * a(2, 2);
    inv_a(0, 0) = a(1, 1) * a(2, 2) - a(1, 2) * a(2, 1);
    inv_a(0, 1) = a(0, 2) * a(2, 1) - a(0, 1) * a(2, 2);
    inv_a(0, 2) = a(0, 1) * a(1, 2) - a(0, 2) * a(1, 1);
    inv_a(1, 0) = a(1, 2) * a(2, 0) - a(1, 0) * a(2, 2);
    inv_a(1, 1) = a(0, 0) * a(2, 2) - a(0, 2) * a(2, 0);
    inv_a(1, 2) = a(0, 2) * a(1, 0) - a(0, 0) * a(1, 2);
    inv_a(2, 0) = a(1, 0) * a(2, 1) - a(1, 1) * a(2, 0);
    inv_a(2, 1) = a(0, 1) * a(2, 0) - a(0, 0) * a(2, 1);
    inv_a(2, 2) = a(0, 0) * a(1, 1) - a(0, 1) * a(1, 0);
        
    auto inv_det = 1. / det;
    FTensor::Index<'i', 3> i;
    FTensor::Index<'j', 3> j;
    inv_a(i, j) *= inv_det;
    return inv_a;
  };

  eigen_vectors(i, j) = strain(i, j); //
  // LAPACK - eigenvalues and vectors. Applied twice for initial creates
  // memory space
  // based on
  // https://software.intel.com/sites/products/documentation/doclib/mkl_sa/11/mkl_lapack_examples/lapacke_zgeev_row.c.htm
  int n = 3, lda = 3, ldvl = 3, ldvr = 3, lwork = -1, info;
  __CLPK_doublecomplex w[3], vl[9], vr[9];
  // double w[3], rwork[3];
  // __CLPK_doublecomplex wkopt;
  __CLPK_doublecomplex wkopt;
  double rwork[6];
  info = lapack_zgeev(
      'N', 'V', n,
      reinterpret_cast<__CLPK_doublecomplex *>(&eigen_vectors(0, 0)),
      lda,                                                          // a
      reinterpret_cast<__CLPK_doublecomplex *>(&(eigen_values(0))), // w
      nullptr, ldvl,                                                // vl
      reinterpret_cast<__CLPK_doublecomplex *>(
          &eigen_vectors_right(0, 0)), // vr
      ldvr, &wkopt, lwork, rwork);
  lwork = (int)wkopt.r;
  __CLPK_doublecomplex work[lwork];

  if (info != 0)
    SETERRQ1(PETSC_COMM_SELF, 1,
             "something is wrong with lapack_zgeev info = %d", info);
  info = lapack_zgeev(
      'N', 'V', n,
      reinterpret_cast<__CLPK_doublecomplex *>(&eigen_vectors(0, 0)),
      lda,                                                          // a
      reinterpret_cast<__CLPK_doublecomplex *>(&(eigen_values(0))), // w
      nullptr, ldvl,                                                // vl
      reinterpret_cast<__CLPK_doublecomplex *>(
          &eigen_vectors_right(0, 0)), // vr
      ldvr, work, lwork, rwork);
  for (int ii : {0, 1, 2}) {
    Aplus(ii, ii) = bracket_plus(eigen_values(ii));
  }
  FTensor::Number<0> N1; ///< x-direction index
  FTensor::Number<1> N2; ///< y-direction index
  FTensor::Number<2> N3; ///< z-direction index

  auto inv_eigen_vec = inverse(eigen_vectors_right);
  // strainP(i, j) = eigen_vectors_right(l, i) * Aplus(l, k) * inv_eigen_vec(j, k);

  strainP(i, j) = eigen_vectors_right(N1, i) * Aplus(N1, N1) * inv_eigen_vec(j, N1);
  strainP(i, j) += eigen_vectors_right(N2, i) * Aplus(N2, N2) * inv_eigen_vec(j, N2);
  strainP(i, j) += eigen_vectors_right(N3, i) * Aplus(N3, N3) * inv_eigen_vec(j, N3);

  MoFEMFunctionReturnHot(0);
}

MoFEMErrorCode PhaseField::CommonData::getComplexDiff(
    const int &k, const int &l, 
    const FTensor::Tensor2<double, 3, 3> &strain,
    FTensor::Tensor2<double, 3, 3> &out) {
  MoFEMFunctionBeginHot;
  FTensor::Tensor2<Cplx, 3, 3> strain_cplx;
  FTensor::Tensor2<Cplx, 3, 3> strainPl;
  FTensor::Index<'I', 3> I;
  FTensor::Index<'J', 3> J;
  strain_cplx(I, J) = strain(I, J);
  const double h = this->h;
  if (k != l) {
    strain_cplx(k, l) = Cplx(strain(k, l), 0.5 * h);
    strain_cplx(l, k) = Cplx(strain(k, l), 0.5 * h);
  } else
    strain_cplx(k, l) = Cplx(strain(k, l), h);
  CHKERR calculateStrainPlusComplex(strain_cplx, strainPl);
  for (int i = 0; i != 3; ++i) {
    for (int j = 0; j != 3; ++j) {
      out(i, j) = std::imag(strainPl(i, j)) / h;
    }
  }
  MoFEMFunctionReturnHot(0);
}

MoFEMErrorCode PhaseField::CommonData::getStrainForFDM(
    const FTensor::Tensor2<double, 3, 3> &strain,
    FTensor::Tensor2_symmetric<double, 3> &strain_diff) { 
  MoFEMFunctionBeginHot;

  FTensor::Tensor2_symmetric<double, 3> strainMinus;
  CHKERR
  strainSpecDecomp<FTensor::Tensor2<double, 3, 3>,
                   FTensor::Tensor2_symmetric<double, 3>>(strain, strain_diff,
                                                          strainMinus);
  MoFEMFunctionReturnHot(0);
};

MoFEMErrorCode PhaseField::CommonData::getFiniteDiff(
    const int &k, const int &l, 
    const FTensor::Tensor2<double, 3, 3> &strain,
    FTensor::Tensor2<double, 3, 3> &out) {
  MoFEMFunctionBeginHot;
  FTensor::Index<'I', 3> I;
  FTensor::Index<'J', 3> J;
  FTensor::Tensor2<double, 3, 3> strainPl = strain;
  FTensor::Tensor2<double, 3, 3> strainMin = strain;
  FTensor::Tensor2_symmetric<double, 3> StressPlusH;
  FTensor::Tensor2_symmetric<double, 3> StressMinusH;
  const double h = this->h;
  if (k != l) {
    strainPl(k, l) = strain(k, l) + 0.5 * h;
    strainPl(l, k) = strain(k, l) + 0.5 * h;
    strainMin(k, l) = strain(k, l) - 0.5 * h;
    strainMin(l, k) = strain(k, l) - 0.5 * h;
  } else {
    strainPl(k, l) += h;
    strainMin(k, l) -= h;
  }
  CHKERR getStrainForFDM(strainPl, StressPlusH);
  CHKERR getStrainForFDM(strainMin, StressMinusH);
  for (int i = 0; i != 3; ++i) {
    for (int j = 0; j != 3; ++j) {
      out(i, j) = (StressPlusH(i, j) - StressMinusH(i, j)) / (2 * h);
    }
  }
  MoFEMFunctionReturnHot(0);
}

MoFEMErrorCode PhaseField::CommonData::getAnaliticalP(
    const FTensor::Tensor2<double, 3, 3> &strain) {
  MoFEMFunctionBeginHot;

  FTensor::Tensor1<double, 3> eigen_values;
  FTensor::Tensor2<double, 3, 3> eigen_vectors;
  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;
  FTensor::Index<'k', 3> k;
  FTensor::Index<'l', 3> l;

  auto bracket_plus = [](auto &a) { return a > 0. ? a : 0; };
  auto bracket_minus = [](auto &a) { return a < 0. ? a : 0; };
  auto heaviside = [](const auto &a) { return a > 0 ? 1 : 0; };

  eigen_vectors(i, j) = strain(j, i);

  // LAPACK - eigenvalues and vectors. Applied twice for initial creates
  // memory space
  int n = 3, lda = 3, info, lwork = -1;
  double wkopt;
  info = lapack_dsyev('V', 'U', n, &(eigen_vectors(0, 0)), lda,
                      &(eigen_values(0)), &wkopt, lwork);
  if (info != 0)
    SETERRQ1(PETSC_COMM_SELF, 1,
             "something is wrong with lapack_dsyev info = %d", info);

  lwork = (int)wkopt;
  double work[lwork];
  info = lapack_dsyev('V', 'U', n, &(eigen_vectors(0, 0)), lda,
                      &(eigen_values(0)), work, lwork);
  if (info != 0)
    SETERRQ1(PETSC_COMM_SELF, 1,
             "something is wrong with lapack_dsyev info = %d", info);

  this->P(i, j, k, l) = 0;

  if(eigen_values(0) == eigen_values(1))
     eigen_values(0) *= (1 + h);
  if(eigen_values(1) == eigen_values(2))
    eigen_values(2) *= (1 - h);

  for (int a : {0, 1, 2}) {
    for (int b : {0, 1, 2}) {
      const double epsilon_a = eigen_values(a);
      const double epsilon_b = eigen_values(b);
      FTensor::Tensor1<double, 3> n_a(eigen_vectors(a, 0), eigen_vectors(a, 1),
                                      eigen_vectors(a, 2));
      FTensor::Tensor1<double, 3> n_b(eigen_vectors(b, 0), eigen_vectors(b, 1),
                                      eigen_vectors(b, 2));

      if(a == b){
        this->P(i, j, k, l) += heaviside(epsilon_a) *
                              (n_a(i) * n_a(j)) * (n_b(k) * n_b(l));
      } else if( epsilon_a - epsilon_b != 0 ){

        const double fraction1 = (bracket_plus(epsilon_a) - bracket_plus(epsilon_b)) /
                           (epsilon_a - epsilon_b);
      
        this->P(i, j, k, l) += 0.5 * fraction1 * n_a(i) * n_b(j) *
                              (n_a(k) * n_b(l) + n_b(k) * n_a(l));
      }
    }
  }
  MoFEMFunctionReturnHot(0);
}
