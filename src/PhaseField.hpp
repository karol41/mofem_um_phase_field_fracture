/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __PHASEFIELDHPP__
#define __PHASEFIELDHPP__

using Cplx = typename std::complex<double>;

auto symm_to_non_tensor = [](const auto &sym, auto &non) {
  non(0, 0) = sym(0, 0);
  non(1, 1) = sym(1, 1);
  non(2, 2) = sym(2, 2);
  non(0, 1) = non(1, 0) = sym(1, 0);
  non(0, 2) = non(2, 0) = sym(2, 0);
  non(1, 2) = non(2, 1) = sym(2, 1);
};

using namespace MoFEM;

struct PhaseField {

  struct CommonData; // that's why header files are important ;-)

  Mat Aij;
  Vec F_res, D, D0;

  DMType dmName;
  DM dm;
  SNES snes;

  MoFEM::Interface &mField;
  CommonData &commonData;
  BitRefLevel bitLevel;

  boost::ptr_map<std::string, NeummanForcesSurface> neumannForces;
  boost::ptr_map<std::string, NodalForce> nodalForces;
  boost::ptr_map<std::string, EdgeForce> edgeForces;

  boost::shared_ptr<VolumeElementForcesAndSourcesCore>
      feLhs; ///< FE to make left hand side
  boost::shared_ptr<VolumeElementForcesAndSourcesCore>
      feRhs; ///< FE to make right hand side
  boost::shared_ptr<VolumeElementForcesAndSourcesCore>
      feUpdate; ///< FE to update history variables
  boost::shared_ptr<DirichletDisplacementBc> dirichletBcPtr;
  boost::shared_ptr<PostProcVolumeOnRefinedMesh> postProcPtr;

  int oRder;              // default approximation order
  int nbSubSteps;         // default number of steps
  int maxDivStep;         // maximumim number of diverged steps
  int outPutStep;         // how often post processing data is saved to h5m file
  int reactionsSidesetId; // id of the sideset for calculating reactions
  double lAmbda0;         // default step size
  double initD;           // initial phase_field value
  double stepRed;         // stepsize reduction while diverging to 20%
  PetscInt testNb;
  PetscBool isAtomTest;
  PetscBool calcReactions;
  PetscBool elasticOnly;
  PetscBool isPartitioned;

  PhaseField(MoFEM::Interface &m_field, CommonData &common_data)
      : mField(m_field), commonData(common_data) {}

  MoFEMErrorCode getParameters();
  MoFEMErrorCode addFields();
  MoFEMErrorCode addElements();
  MoFEMErrorCode addMomentumFluxes();
  MoFEMErrorCode buildDM();
  MoFEMErrorCode setOperators();
  MoFEMErrorCode setupBoundaryConditions();
  MoFEMErrorCode setupProblem();
  MoFEMErrorCode setupSolver();
  MoFEMErrorCode solveDM();
  MoFEMErrorCode calcReactionForces(VectorDouble &reactions);
  MoFEMErrorCode atomTests(const int step_number,
                           const VectorDouble &reactions);

  struct BlockData {
    int iD;
    int oRder;
    double yOung;
    double pOisson;
    Range tEts;
    BlockData() : oRder(-1), yOung(-1), pOisson(-2) {}
  };

  struct LoadScale : public MethodForForceScaling {

    static double lambda;

    MoFEMErrorCode scaleNf(const FEMethod *fe, VectorDouble &nf) {
      MoFEMFunctionBegin;
      nf *= lambda;
      MoFEMFunctionReturn(0);
    }
  };

  // declaration
  struct CommonData {

    boost::shared_ptr<MatrixDouble> gradDispPtr;
    boost::shared_ptr<MatrixDouble> sTressPtr;
    boost::shared_ptr<MatrixDouble> sTrainPtr;
    boost::shared_ptr<MatrixDouble> stressPlusPtr;
    boost::shared_ptr<MatrixDouble> gradcPtr;
    boost::shared_ptr<VectorDouble> cPtr;
    boost::shared_ptr<VectorDouble> historyVariablePtr;
    boost::shared_ptr<VectorDouble> psiPtr;
    FTensor::Tensor4<double, 3, 3, 3, 3> P;

    FTensor::Tensor4<int, 3, 3, 3, 3> unitTensorIJKL;
    FTensor::Tensor4<double, 3, 3, 3, 3> unitTensorSYM;

    double pOisson; //< young modulus
    double yOung;   //< poisson ration
    double gC;      //< fracture energy
    double l0;      //< length scale parameter

    double h; //< eps
    double k; //< for degr function
    double lAmbda;
    double mU;
    int pFunctionNb;
    std::map<int, BlockData> setOfBlocksData;

    CommonData(MoFEM::Interface &m_field) : mField(m_field) {

      // Setting default values for coeffcients
      pFunctionNb = 1;
      gC = 0.01;
      h = 1e-8;
      k = 1e-6;
      l0 = 0.05;
      pOisson = 0.0;
      yOung = 1.;
      gradDispPtr = boost::make_shared<MatrixDouble>();
      sTressPtr = boost::make_shared<MatrixDouble>();
      sTrainPtr = boost::make_shared<MatrixDouble>();
      stressPlusPtr = boost::make_shared<MatrixDouble>();
      cPtr = boost::make_shared<VectorDouble>();
      gradcPtr = boost::make_shared<MatrixDouble>();
      psiPtr = boost::make_shared<VectorDouble>();
      historyVariablePtr = boost::make_shared<VectorDouble>();

      ierr = setBlocks();
      CHKERRABORT(PETSC_COMM_WORLD, ierr);
      ierr = createTags(m_field.get_moab());
      CHKERRABORT(PETSC_COMM_WORLD, ierr);

    FTensor::Index<'i', 3> i;
    FTensor::Index<'j', 3> j;
    FTensor::Index<'k', 3> k;
    FTensor::Index<'l', 3> l;

    const FTensor::Tensor2<int, 3, 3> D_K(1, 0, 0, 0, 1, 0, 0, 0, 1);
    unitTensorSYM(i, j, k, l) =
        0.5 * D_K(i, k) * D_K(j, l) + 0.5 * D_K(i, l) * D_K(j, k);
    unitTensorIJKL(i, j, k, l) = D_K(i, j) * D_K(k, l);
    }

    template <class T1, class T2>
    MoFEMErrorCode strainSpecDecomp(const T1 &strain, T2 &positive_strain,
                                    T2 &negative_strain);

    MoFEMErrorCode
    calculateStrainPlusComplex(const FTensor::Tensor2<Cplx, 3, 3> &strain,
                        FTensor::Tensor2<Cplx, 3, 3> &strainP);

    MoFEMErrorCode getComplexDiff(const int &k, const int &l,
                                  const FTensor::Tensor2<double, 3, 3> &strain,
                                  FTensor::Tensor2<double, 3, 3> &out);
    MoFEMErrorCode getFiniteDiff(const int &k, const int &l,
                                 const FTensor::Tensor2<double, 3, 3> &strain,
                                 FTensor::Tensor2<double, 3, 3> &out);
    MoFEMErrorCode getStrainForFDM(const FTensor::Tensor2<double, 3, 3> &strain,
                                   FTensor::Tensor2_symmetric<double, 3> &strain_diff);
    MoFEMErrorCode
    getAnaliticalP(const FTensor::Tensor2<double, 3, 3> &strain);

    MoFEMErrorCode getParameters() {
      MoFEMFunctionBegin;

      CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "Phase field mesh:", "none");
      CHKERR PetscOptionsScalar("-young", "Young modulus", "", yOung, &yOung,
                                PETSC_NULL);
      CHKERR PetscOptionsScalar("-poisson", "Poisson ratio", "", pOisson,
                                &pOisson, PETSC_NULL);
      CHKERR PetscOptionsScalar("-gc", "Fracture energy", "", gC, &gC,
                                PETSC_NULL);
      CHKERR PetscOptionsScalar("-l0", "Length scale parameter.", "", l0, &l0,
                                PETSC_NULL);
      CHKERR PetscOptionsScalar("-eps_h", "Epsilon parameter", "", h, &h,
                                PETSC_NULL);
      CHKERR PetscOptionsScalar("-eps_k", "Epsilon parameter", "", k, &k,
                                PETSC_NULL);
      CHKERR PetscOptionsInt("-p_func",
                             "Number of the function to calculate projection P "
                             "tensor (1 - complex, "
                             "2 - finite diff, 3 - analitical)",
                             "", pFunctionNb, &pFunctionNb, PETSC_NULL);

      ierr = PetscOptionsEnd();
      CHKERRQ(ierr);
      MoFEMFunctionReturn(0);
    }

    inline double degrFuncSt(const double &g) {
      return (1 - g) * (1 - g) + this->k;
    } // stay safe
    inline double degrFuncClr(const double &g) { return (1 - g) * (1 - g); }
    inline double diffDegrFunc(const double &g) { return 2 * (g - 1); }
    inline double diffdiffDegrFunc(const double &g) { return 2; }

    MoFEMErrorCode getBlockData(BlockData &data) {
      MoFEMFunctionBegin;

      yOung = data.yOung;
      pOisson = data.pOisson;
      lAmbda = (yOung * pOisson) / ((1. + pOisson) * (1. - 2. * pOisson));
      mU = yOung / (2. * (1. + pOisson));

      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;
      FTensor::Index<'k', 3> k;
      FTensor::Index<'l', 3> l;

      MoFEMFunctionReturn(0);
    }

    MoFEMErrorCode setBlocks() {
      MoFEMFunctionBegin;
      for (_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(
               mField, BLOCKSET | MAT_ELASTICSET, it)) {
        Mat_Elastic mydata;
        CHKERR it->getAttributeDataStructure(mydata);
        int id = it->getMeshsetId();
        EntityHandle meshset = it->getMeshset();
        CHKERR mField.get_moab().get_entities_by_type(
            meshset, MBTET, setOfBlocksData[id].tEts, true);
        setOfBlocksData[id].iD = id;
        setOfBlocksData[id].yOung = mydata.data.Young;
        setOfBlocksData[id].pOisson = mydata.data.Poisson;
      }
      MoFEMFunctionReturn(0);
    }

    MoFEMErrorCode getHistoryVar(const EntityHandle fe_ent,
                                 const int nb_gauss_pts) {
      MoFEMFunctionBegin;
      double *tag_data;
      int tag_size;
      CHKERR mField.get_moab().tag_get_by_ptr(
          thHistoryVar, &fe_ent, 1, (const void **)&tag_data, &tag_size);

      if (tag_size == 1) {
        historyVariablePtr->resize(nb_gauss_pts);
        historyVariablePtr->clear();
        void const *tag_data[] = {&*historyVariablePtr->begin()};
        const int tag_size = historyVariablePtr->size();
        CHKERR mField.get_moab().tag_set_by_ptr(thHistoryVar, &fe_ent, 1,
                                                tag_data, &tag_size);
      } else if (tag_size != nb_gauss_pts) {
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                "Wrong size of the tag, data inconsistency");
      } else {
        VectorAdaptor tag_vec = VectorAdaptor(
            tag_size, ublas::shallow_array_adaptor<double>(tag_size, tag_data));
        *historyVariablePtr = tag_vec;
      }
      MoFEMFunctionReturn(0);
    }

    MoFEMErrorCode setHistoryVar(const EntityHandle fe_ent,
                                 const int nb_gauss_pts) {
      MoFEMFunctionBegin;
      void const *tag_data[] = {&*historyVariablePtr->begin()};
      const int tag_size = historyVariablePtr->size();
      CHKERR mField.get_moab().tag_set_by_ptr(thHistoryVar, &fe_ent, 1,
                                              tag_data, &tag_size);
      MoFEMFunctionReturn(0);
    }

  private:
    MoFEM::Interface &mField;
    Tag thHistoryVar;

    MoFEMErrorCode createTags(moab::Interface &moab) {
      MoFEMFunctionBegin;
      double def_val = 0.0;
      CHKERR mField.get_moab().tag_get_handle(
          "HISTORY_VAR", 1, MB_TYPE_DOUBLE, thHistoryVar,
          MB_TAG_CREAT | MB_TAG_VARLEN | MB_TAG_SPARSE, &def_val);
      MoFEMFunctionReturn(0);
    }
  };
  /**
   * @brief Assemble K^dd
   *
   * \f[
   * \mathbf{K}^{dd} = \int_\Omega \mathcal G_c \left(\frac{1}{2l_0} N_d^T N_d +
   * \mathbf{ B}_d^T  \mathbf{B}_d   \right) + \frac{\partial^2 g}{\partial d^2}
   * \mathcal H N_d^T \, \mathrm d V \f]
   *
   */
  struct OpAssembleDLhs_dd
      : public VolumeElementForcesAndSourcesCore::UserDataOperator {

    CommonData &commonData;
    MatrixDouble locK_d_d;
    MatrixDouble translocK_d_d;
    BlockData &dAta;

    OpAssembleDLhs_dd(CommonData &common_data, BlockData &data)
        : VolumeElementForcesAndSourcesCore::UserDataOperator(
              "PHASE_FIELD", "PHASE_FIELD", UserDataOperator::OPROWCOL),
          commonData(common_data), dAta(data) {
      sYmm = true;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type,
                          DataForcesAndSourcesCore::EntData &row_data,
                          DataForcesAndSourcesCore::EntData &col_data) {

      MoFEMFunctionBegin;

      const int row_nb_dofs = row_data.getIndices().size();
      if (!row_nb_dofs)
        MoFEMFunctionReturnHot(0);
      const int col_nb_dofs = col_data.getIndices().size();
      if (!col_nb_dofs)
        MoFEMFunctionReturnHot(0);

      if (dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) ==
          dAta.tEts.end()) {
        MoFEMFunctionReturnHot(0);
      }
      CHKERR commonData.getBlockData(dAta);
      // Set size can clear local tangent matrix
      locK_d_d.resize(row_nb_dofs, col_nb_dofs, false);
      locK_d_d.clear();
      const int row_nb_gauss_pts = row_data.getN().size1();
      if (!row_nb_gauss_pts)
        MoFEMFunctionReturnHot(0);
      const int row_nb_base_functions = row_data.getN().size2();
      auto row_base_functions = row_data.getFTensor0N();
      auto row_diff_base_functions = row_data.getFTensor1DiffN<3>();

      // get data
      FTensor::Index<'i', 3> i;
      const double lambda = commonData.lAmbda;
      const double mu = commonData.mU;
      const double gc = commonData.gC;
      const double l = commonData.l0;
      auto psi_plus = getFTensor0FromVec(*commonData.psiPtr);
      auto d = getFTensor0FromVec(*commonData.cPtr);

      for (int gg = 0; gg != row_nb_gauss_pts; gg++) {

        // Get volume and integration weight
        double w = getVolume() * getGaussPts()(3, gg);
        if (getHoGaussPtsDetJac().size() > 0) {
          w *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
        }

        const double aa = gc * w * (1. / l);
        const double bb = gc * w * l;
        const double cc = w * commonData.diffdiffDegrFunc(d) * psi_plus;

        // INTEGRATION
        int row_bb = 0;
        for (; row_bb != row_nb_dofs; row_bb++) {
          auto col_base_functions = col_data.getFTensor0N(gg, 0);
          auto col_diff_base_functions = col_data.getFTensor1DiffN<3>(gg, 0);
          for (int col_bb = 0; col_bb != col_nb_dofs; col_bb++) {
            locK_d_d(row_bb, col_bb) +=
                aa * row_base_functions * col_base_functions;
            locK_d_d(row_bb, col_bb) +=
                bb * row_diff_base_functions(i) * col_diff_base_functions(i);
            locK_d_d(row_bb, col_bb) +=
                cc * row_base_functions * col_base_functions;
            ++col_base_functions;
            ++col_diff_base_functions;
          }
          ++row_base_functions;
          ++row_diff_base_functions;
        }
        for (; row_bb != row_nb_base_functions; row_bb++) {
          ++row_base_functions;
          ++row_diff_base_functions;
        }

        ++psi_plus;
        ++d;
      }

      CHKERR MatSetValues(getFEMethod()->snes_B, row_nb_dofs,
                          &*row_data.getIndices().begin(), col_nb_dofs,
                          &*col_data.getIndices().begin(), &locK_d_d(0, 0),
                          ADD_VALUES);

      // is symmetric
      if (row_side != col_side || row_type != col_type) {
        translocK_d_d.resize(col_nb_dofs, row_nb_dofs, false);
        noalias(translocK_d_d) = trans(locK_d_d);
        CHKERR MatSetValues(getFEMethod()->snes_B, col_nb_dofs,
                            &*col_data.getIndices().begin(), row_nb_dofs,
                            &*row_data.getIndices().begin(),
                            &translocK_d_d(0, 0), ADD_VALUES);
      }

      MoFEMFunctionReturn(0);
    }
  };
  /**
   * @brief Assemble K^du
   *
   *
   * \f[
   * \mathbf{K}^{du} = \int_\Omega \frac{\partial g}{\partial d} N
   * \frac{\partial \mathcal H}{\partial \mathbf{ \epsilon}} \mathbf{B}_u \,
   * \mathrm d V \\ \frac{\partial \mathcal H}{\partial \mathbf \epsilon} =
   * \lambda \langle \mathrm{tr}(\mathbf \epsilon) \rangle \mathcal I + 2 \mu
   * \mathbf \epsilon_{+} \f]
   *
   */
  struct OpAssembleDLhs_dU
      : public VolumeElementForcesAndSourcesCore::UserDataOperator {

    CommonData &commonData;
    MatrixDouble locK_d_u;
    BlockData &dAta;

    OpAssembleDLhs_dU(CommonData &common_data, BlockData &data)
        : VolumeElementForcesAndSourcesCore::UserDataOperator(
              "PHASE_FIELD", "U", UserDataOperator::OPROWCOL),
          commonData(common_data), dAta(data) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type,
                          DataForcesAndSourcesCore::EntData &row_data,
                          DataForcesAndSourcesCore::EntData &col_data) {

      MoFEMFunctionBegin;
      const int row_nb_dofs = row_data.getIndices().size();
      if (!row_nb_dofs)
        MoFEMFunctionReturnHot(0);
      const int col_nb_dofs = col_data.getIndices().size();
      if (!col_nb_dofs)
        MoFEMFunctionReturnHot(0);
      if (dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) ==
          dAta.tEts.end()) {
        MoFEMFunctionReturnHot(0);
      }
      CHKERR commonData.getBlockData(dAta);

      // Set size can clear local tangent matrix
      locK_d_u.resize(row_nb_dofs, col_nb_dofs, false);
      locK_d_u.clear();
      const int row_nb_gauss_pts = row_data.getN().size1();
      if (!row_nb_gauss_pts)
        MoFEMFunctionReturnHot(0);

      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;
      auto t_stress_plus = getFTensor2SymmetricFromMat<3>(*commonData.stressPlusPtr);;

      double lambda = commonData.lAmbda;
      double mu = commonData.mU;
      const int row_nb_base_functions = row_data.getN().size2();
      const int col_nb_base_functions = col_data.getN().size2();
      auto col_diff_base_functions = col_data.getFTensor1DiffN<3>();

      auto psi_plus = getFTensor0FromVec(*commonData.psiPtr);
      auto H = getFTensor0FromVec(*commonData.historyVariablePtr);

      auto bracket_plus = [](auto a) { return a > 0.0 ? a : 0.0; };
      // get data
      FTensor::Tensor2_symmetric<double, 3> psi_diff;

      for (int gg = 0; gg != row_nb_gauss_pts; gg++) {

        // Get volume and integration weight
        double w = getVolume() * getGaussPts()(3, gg);
        if (getHoGaussPtsDetJac().size() > 0) {
          w *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
        }
        if (psi_plus > H) {
          psi_diff(i, j) = t_stress_plus(i, j);

        } else {
          psi_diff(i, j) = 0;
        }
        int col_bb = 0;
        for (; col_bb != col_nb_dofs / 3; col_bb++) {

          auto row_base_functions = row_data.getFTensor0N(gg, 0);
          for (int row_bb = 0; row_bb != row_nb_dofs; row_bb++) {
            FTensor::Tensor1<double *, 3> k(&locK_d_u(row_bb, 3 * col_bb + 0),
                                            &locK_d_u(row_bb, 3 * col_bb + 1),
                                            &locK_d_u(row_bb, 3 * col_bb + 2));

            k(i) += w * row_base_functions * psi_diff(i, j) *
                    col_diff_base_functions(j);
            ++row_base_functions;
          }
          ++col_diff_base_functions;
        }
        for (; col_bb != col_nb_base_functions; col_bb++) {

          ++col_diff_base_functions;
        }

        ++t_stress_plus;
        ++psi_plus;

        ++H;
      }

      CHKERR MatSetValues(getFEMethod()->snes_B, row_nb_dofs,
                          &*row_data.getIndices().begin(), col_nb_dofs,
                          &*col_data.getIndices().begin(), &locK_d_u(0, 0),
                          ADD_VALUES);

      MoFEMFunctionReturn(0);
    }
  };
  /**
   * @brief calculate K^ud
   *
   * \f[
   * \mathbf{K}^{ud} = \int_\Omega \mathbf{B}_u^T \frac{\partial \mathbf
   * \sigma}{\partial d} N \, \mathrm d V \\
   * \frac{\partial \mathbf \sigma}{\partial d} = \frac{\partial g}{\partial d}
   * \left( \lambda \langle \mathrm{tr}(\mathbf \epsilon) \rangle \mathcal I + 2
   * \mu \mathbf \epsilon_{+} \right) \\ \f]
   *
   */
  struct OpAssembleULhs_dd
      : public VolumeElementForcesAndSourcesCore::UserDataOperator {

    CommonData &commonData;
    MatrixDouble locK_u_d;
    BlockData &dAta;

    OpAssembleULhs_dd(CommonData &common_data, BlockData &data)
        : VolumeElementForcesAndSourcesCore::UserDataOperator(
              "U", "PHASE_FIELD", UserDataOperator::OPROWCOL),
          commonData(common_data), dAta(data) {
      sYmm = false;
    }

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type,
                          DataForcesAndSourcesCore::EntData &row_data,
                          DataForcesAndSourcesCore::EntData &col_data) {

      MoFEMFunctionBegin;

      const int row_nb_dofs = row_data.getIndices().size();
      if (!row_nb_dofs)
        MoFEMFunctionReturnHot(0);
      const int col_nb_dofs = col_data.getIndices().size();
      if (!col_nb_dofs)
        MoFEMFunctionReturnHot(0);

      if (dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) ==
          dAta.tEts.end()) {
        MoFEMFunctionReturnHot(0);
      }
      CHKERR commonData.getBlockData(dAta);

      // Set size can clear local tangent matrix
      locK_u_d.resize(row_nb_dofs, col_nb_dofs, false);
      locK_u_d.clear();

      const int row_nb_gauss_pts = row_data.getN().size1();
      if (!row_nb_gauss_pts)
        MoFEMFunctionReturnHot(0);
      const int row_nb_base_functions = row_data.getN().size2();
      auto row_diff_base_functions = row_data.getFTensor1DiffN<3>();


      const double lambda = commonData.lAmbda;
      const double mu = commonData.mU;
      auto t_stress_plus =
          getFTensor2SymmetricFromMat<3>(*commonData.stressPlusPtr);

      FTensor::Tensor1<double, 3> t1;
      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;
      auto bracket_plus = [](auto &a) { return a > 0. ? a : 0.; };

      for (int gg = 0; gg != row_nb_gauss_pts; gg++) {

        // Get volume and integration weight
        double w = getVolume() * getGaussPts()(3, gg);
        if (getHoGaussPtsDetJac().size() > 0) {
          w *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
        }

        int row_bb = 0;
        for (; row_bb != row_nb_dofs / 3; row_bb++) {
          t1(i) = w * row_diff_base_functions(j) * t_stress_plus(i, j);

          auto base_functions = col_data.getFTensor0N(gg, 0);
          for (int col_bb = 0; col_bb != col_nb_dofs; col_bb++) {
            FTensor::Tensor1<double *, 3> k(&locK_u_d(3 * row_bb + 0, col_bb),
                                            &locK_u_d(3 * row_bb + 1, col_bb),
                                            &locK_u_d(3 * row_bb + 2, col_bb));
            k(i) += t1(i) * base_functions;
            ++base_functions;
          }
          ++row_diff_base_functions;
        }
        for (; row_bb != row_nb_base_functions; row_bb++) {
          ++row_diff_base_functions;
        }

        ++t_stress_plus;
      }

      CHKERR MatSetValues(getFEMethod()->snes_B, row_nb_dofs,
                          &*row_data.getIndices().begin(), col_nb_dofs,
                          &*col_data.getIndices().begin(),
                          &*locK_u_d.data().begin(), ADD_VALUES);

      MoFEMFunctionReturn(0);
    }
  };
  /**
   * @brief assemble K^uu
   *
   *
   * /f[
   *
   *
   * \mathbf \epsilon_+ = \mathbb P^+ \mathbf \epsilon \\
  \langle x \rangle = \begin{cases} 0 \quad x \leq 0 \\
  x \quad x > 0
  \end{cases} \\
  \mathbf \sigma(d, \mathbf \epsilon) = \mathrm g (d) \left[ \lambda \langle
  \mathrm{tr}( \mathbf \epsilon) \rangle \mathbb \mathbf 1 + 2 \mu \mathbf
  \epsilon_+ \right] + \left[ \lambda \langle - \mathrm{tr}( \mathbf \epsilon)
  \rangle \mathbb \mathbf 1 + 2 \mu (\mathbf \epsilon - \mathbb P^+ \mathbf
  \epsilon) \right] \\ \frac{\partial \mathbf \sigma}{ \partial \mathbf
  \epsilon} = ?
   * /f]
   *
   *
   *
   * \f[
   * \mathbf \epsilon_+ = \mathbb P^+ \mathbf \epsilon \\
   * \mathbb P_{ijkl} = \frac{\partial \epsilon^{+}_{ij}}{\partial
  \epsilon_{kl}} \approx Im(\sigma_{ij}(\epsilon + i h \mathbb E_{kl})) / h
   * \mathbf \sigma(d, \mathbf \epsilon) = \mathrm g (d) \left[ \lambda \langle
  \mathrm{tr}( \mathbf \epsilon) \rangle \mathbb \mathbf 1 + 2 \mu \mathbf
  \epsilon_+ \right] + \left[ \lambda \langle - \mathrm{tr}( \mathbf \epsilon)
  \rangle \mathbb \mathbf 1 + 2 \mu (\mathbf \epsilon - \mathbf \epsilon_+)
  \right] \\
   *  \mathbf{K}^{uu} = \int_\Omega \mathbf{B}_u^T \frac{\partial \mathbf
  \sigma}{\partial \mathbf \epsilon} \mathbf{B}_u \, \mathrm d V \\
   *  \frac{\partial \mathbf \sigma}{\partial \mathbf \epsilon} = \lambda \left[
  g(d) \mathrm H ( \mathrm{tr} \mathbf \epsilon) - \mathrm H(-\mathrm{tr}
  \mathbf \epsilon) \right] \mathbb{I} \otimes \mathbb{I} + 2 \mu \left( g(d) -
  1 \right) \mathbb P^+ + \mu \left( \mathbb{II} + \mathbb{II}^{\mathrm s}
  \right) \\ \frac{\partial \mathbf \sigma}{\partial \mathbf \epsilon} = \lambda
  \left[ g(d) \mathrm H ( \mathbf \epsilon_{ii}) - \mathrm H(-\mathbf
  \epsilon_{ii}) \right] \delta_{ij} \delta_{kl} + 2 \mu \left( g(d) - 1 \right)
  \mathbb P_{ijkl}^+ + \mu \left( \delta_{ik} \delta_{jl} + \delta_{il}
  \delta_{jk} \right)
   * \f]
   *
   */
  struct OpAssembleULhs_dU
      : public VolumeElementForcesAndSourcesCore::UserDataOperator {

    MatrixDouble locK_u_u;
    BlockData &dAta;
    FTensor::Tensor2<double, 3, 3> diffDiff;
    bool isDiag;
    CommonData &commonData;

    OpAssembleULhs_dU(CommonData &common_data, BlockData &data,
                      bool symm = true)
        : VolumeElementForcesAndSourcesCore::UserDataOperator("U", "U",
                                                              OPROWCOL, symm),
          commonData(common_data), dAta(data) {}

    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type,
                          DataForcesAndSourcesCore::EntData &row_data,
                          DataForcesAndSourcesCore::EntData &col_data) {
      MoFEMFunctionBegin;

      auto get_tensor2 = [](MatrixDouble &m, const int r, const int c) {
        return FTensor::Tensor2<FTensor::PackPtr<double *, 3>, 3, 3>(
            &m(r + 0, c + 0), &m(r + 0, c + 1), &m(r + 0, c + 2),
            &m(r + 1, c + 0), &m(r + 1, c + 1), &m(r + 1, c + 2),
            &m(r + 2, c + 0), &m(r + 2, c + 1), &m(r + 2, c + 2));
      };

      const int row_nb_dofs = row_data.getIndices().size();
      if (!row_nb_dofs)
        MoFEMFunctionReturnHot(0);
      const int col_nb_dofs = col_data.getIndices().size();
      if (!col_nb_dofs)
        MoFEMFunctionReturnHot(0);
      if (dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) ==
          dAta.tEts.end()) {
        MoFEMFunctionReturnHot(0);
      }
      commonData.getBlockData(dAta);

      if (row_side == col_side && row_type == col_type) {
        isDiag = true;
      } else {
        isDiag = false;
      }

      // get number of integration points
      // Set size can clear local tangent matrix
      locK_u_u.resize(row_nb_dofs, col_nb_dofs, false);
      locK_u_u.clear();

      const int row_nb_gauss_pts = row_data.getN().size1();
      const int row_nb_base_functions = row_data.getN().size2();

      auto d = getFTensor0FromVec(*commonData.cPtr);
      auto row_diff_base_functions = row_data.getFTensor1DiffN<3>();
      auto t_strain = getFTensor2SymmetricFromMat<3>(*commonData.sTrainPtr);

      const double mu = commonData.mU;
      const double lambda = commonData.lAmbda;
      FTensor::Tensor2<double, 3, 3> strainT;
      FTensor::Tensor4<double, 3, 3, 3, 3> matTangent;

      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;
      FTensor::Index<'k', 3> k;
      FTensor::Index<'l', 3> l;

      auto heaviside = [](const auto &a) { return a >= 0 ? 1 : 0; };
      auto h_h = [](const auto &tr, const auto g) { return tr > 0 ? g : 1; };
      int iter = 0;

      double (CommonData::*dgr_fun_ptr)(const double &);
      CHKERR SNESGetIterationNumber(getFEMethod()->snes, &iter);
      if (iter != 0) {
        dgr_fun_ptr = &CommonData::degrFuncClr;
      } else {
        dgr_fun_ptr = &CommonData::degrFuncSt;
      }

      std::function<MoFEMErrorCode()> get_p_tensor_function;

      auto calc_P_tensor = [&](MoFEMErrorCode (CommonData::*my_p_calc)(
                               const int &, const int &,
                               const FTensor::Tensor2<double, 3, 3> &,
                               FTensor::Tensor2<double, 3, 3> &)) {
        MoFEMFunctionBeginHot;
        for (int k = 0; k != 3; ++k) {
          for (int l = 0; l <= k; ++l) {
            FTensor::Tensor2<double, 3, 3> strainPPL;
            // commonData.getComplexDiff(k, l, g_d, strainT, strainPPL);
            // commonData.getFiniteDiff(k, l, g_d, strainT, strainPPL);
            (commonData.*my_p_calc)(k, l, strainT, strainPPL);
            for (int i = 0; i != 3; ++i) {
              for (int j = 0; j != 3; ++j) {
                commonData.P(i, j, k, l) = strainPPL(i, j);
                if (k != l)
                  commonData.P(i, j, l, k) = strainPPL(i, j);
              }
            }
          }
        }
        MoFEMFunctionReturnHot(0);
      };

      switch (commonData.pFunctionNb) {
      case 1:
        get_p_tensor_function = [&]() {
          MoFEMFunctionBeginHot;
          CHKERR calc_P_tensor(&CommonData::getComplexDiff);
          MoFEMFunctionReturnHot(0);
        };
        break;
      case 2:
        get_p_tensor_function = [&]() {
          MoFEMFunctionBeginHot;
          CHKERR calc_P_tensor(&CommonData::getFiniteDiff);
          MoFEMFunctionReturnHot(0);
        };
        break;
      case 3:
        get_p_tensor_function = [&]() {
          MoFEMFunctionBeginHot;
          CHKERR commonData.getAnaliticalP(strainT);
          MoFEMFunctionReturnHot(0);
        };
        break;
      default:
        get_p_tensor_function = [&]() {
          MoFEMFunctionBeginHot;
          CHKERR commonData.getAnaliticalP(strainT);
          MoFEMFunctionReturnHot(0);
        };
        break;
      }

      // integrate local matrix for entity block
      for (int gg = 0; gg != row_nb_gauss_pts; gg++) {

        // Get volume and integration weight
        double w = getVolume() * getGaussPts()(3, gg);
        if (getHoGaussPtsDetJac().size() > 0) {
          w *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
        }

        symm_to_non_tensor(t_strain, strainT);
        CHKERR get_p_tensor_function();
        const double g_d = (commonData.*dgr_fun_ptr)(d);

        const double trace = t_strain(i, i);
        const int heavyside_plus = heaviside(trace);
        const int heavyside_minus = heaviside(-trace);

        for (int I = 0; I != 3; ++I) {
          for (int J = 0; J != 3; ++J) {
            for (int K = 0; K != 3; ++K) {
              for (int L = 0; L != 3; ++L) {
                // double unit_ten = D_K(I, K) * D_K(J, L);
                // double unit = D_K(I, J) * D_K(K, L);
                // double I_s = D_K(I, L) * D_K(J, K);
                // matTangent(I, J, K, L) = 0.;
                // FIXME: very inefficient
                // const double sym_tensor =
                //     0.5 * D_K(I, K) * D_K(J, L) + 0.5 * D_K(I, L) * D_K(J,
                //     K);
                matTangent(I, J, K, L) =
                    commonData.unitTensorIJKL(I, J, K, L) * heavyside_plus *
                        lambda * g_d +
                    2 * mu * commonData.P(I, J, K, L) * g_d;
                matTangent(I, J, K, L) +=
                    commonData.unitTensorIJKL(I, J, K, L) * heavyside_minus *
                        lambda +
                    2 * mu *
                        (commonData.unitTensorSYM(I, J, K, L) -
                         commonData.P(I, J, K, L));
              }
            }
          }
        }

        int row_bb = 0;
        for (; row_bb != row_nb_dofs / 3; row_bb++) {

          auto t_assemble = get_tensor2(locK_u_u, 3 * row_bb, 0);

          auto col_diff_base_functions = col_data.getFTensor1DiffN<3>(gg, 0);
          int col_bb = 0;
          for (; col_bb != col_nb_dofs / 3; col_bb++) {

            diffDiff(j, l) =
                w * row_diff_base_functions(j) * col_diff_base_functions(l);

            t_assemble(i, k) += diffDiff(j, l) * matTangent(i, j, k, l); //

            // Next base function for column
            ++col_diff_base_functions;
            ++t_assemble;
          }

          ++row_diff_base_functions;
        }
        for (; row_bb != row_nb_base_functions; row_bb++) {
          ++row_diff_base_functions;
        }

        // ++strain;
        ++t_strain;
        ++d;
      }

      const int *row_ind = &*row_data.getIndices().begin();
      const int *col_ind = &*col_data.getIndices().begin();
      Mat B = getFEMethod()->ksp_B != PETSC_NULL ? getFEMethod()->ksp_B
                                                 : getFEMethod()->snes_B;
      CHKERR MatSetValues(B, row_nb_dofs, row_ind, col_nb_dofs, col_ind,
                          &locK_u_u(0, 0), ADD_VALUES);

      if (!isDiag && sYmm) {
        // if not diagonal term and since global matrix is symmetric assemble
        // transpose term.
        locK_u_u = trans(locK_u_u);
        CHKERR MatSetValues(B, col_nb_dofs, col_ind, row_nb_dofs, row_ind,
                            &locK_u_u(0, 0), ADD_VALUES);
      }

      MoFEMFunctionReturn(0);
    }
  };

  /**
   * @brief calculate stress and energy
   *    \f[
            \psi^e(\epsilon,d) = g(d) \psi^d(\epsilon) + \psi^i(\epsilon) \\
            \mathbf{\sigma}( \mathbf{\epsilon}, d) = g(d) \frac{\psi^d}{\partial
   \mathbf{\epsilon}} + \frac{\psi^i}{\partial \mathbf{\epsilon}} \\
            \mathbf{\sigma}( \mathbf{\epsilon}, d) = g(d) [ \lambda \langle
   \mathrm{tr} \epsilon \rangle \mathbb I  + 2 \mu \epsilon^+] + \lambda \langle
   - \mathrm{tr} \epsilon \rangle \mathbb I + 2 \mu \epsilon^- \\ \f]
   */
  struct OpCalculateStress
      : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

    MoFEM::Interface &mField;
    CommonData &commonData;
    BlockData &dAta;
    bool uPdate;
    OpCalculateStress(MoFEM::Interface &m_field, string field_name,
                      CommonData &common_data, BlockData &data,
                      bool update = false)
        : MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
              field_name, UserDataOperator::OPROW),
          mField(m_field), commonData(common_data), dAta(data), uPdate(update) {
      doEdges = false;
      doQuads = false;
      doTris = false;
      doTets = false;
      doPrisms = false;
    }

    MoFEMErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;

      int nb_dofs = data.getFieldData().size();
      if (nb_dofs == 0)
        MoFEMFunctionReturnHot(0);

      if (dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) ==
          dAta.tEts.end()) {
        MoFEMFunctionReturnHot(0);
      }
      CHKERR commonData.getBlockData(dAta);

      EntityHandle fe_ent = getFEEntityHandle();
      int nb_gauss_pts = data.getN().size1();
      const int nb_base_functions = data.getN().size2();
      auto diff_base_functions = data.getFTensor1DiffN<3>();
      auto t_grad = getFTensor2FromMat<3,3>(*commonData.gradDispPtr);

      commonData.sTrainPtr.get()->resize(6, nb_gauss_pts, false);
      commonData.sTrainPtr.get()->clear();
      auto t_strain = getFTensor2SymmetricFromMat<3>(*commonData.sTrainPtr);

      commonData.stressPlusPtr.get()->resize(6, nb_gauss_pts, false);
      commonData.stressPlusPtr.get()->clear();
      auto t_stress_plus = getFTensor2SymmetricFromMat<3>(*commonData.stressPlusPtr);

      commonData.sTressPtr.get()->resize(6, nb_gauss_pts, false);
      commonData.sTressPtr.get()->clear();
      auto t_stress = getFTensor2SymmetricFromMat<3>(*commonData.sTressPtr);

      commonData.psiPtr.get()->resize(nb_gauss_pts, false);
      commonData.psiPtr.get()->clear();
      auto psi_plus = getFTensor0FromVec(*commonData.psiPtr);
      auto d = getFTensor0FromVec(*commonData.cPtr);

      CHKERR commonData.getHistoryVar(fe_ent, nb_gauss_pts);
      auto H = getFTensor0FromVec(*commonData.historyVariablePtr);

      const double lambda = commonData.lAmbda;
      const double mu = commonData.mU;
      double (CommonData::*dgr_fun_ptr)(const double &);
      int iter;
      CHKERR SNESGetIterationNumber(getFEMethod()->snes, &iter); //perturb degradation function at first iteration
      if (iter != 0) {
        dgr_fun_ptr = &CommonData::degrFuncClr;
      } else {
        dgr_fun_ptr = &CommonData::degrFuncSt;
      }

      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;
      FTensor::Index<'k', 3> k;
      FTensor::Index<'l', 3> l;
      FTensor::Tensor2_symmetric<double, 3> strain_plus;
      FTensor::Tensor2_symmetric<double, 3> strain_minus;

      // Macaulay bracket
      auto bracket_plus = [](auto &a) { return a > 0. ? a : 0; };
      auto bracket_minus = [](auto &a) { return a < 0. ? a : 0; };
      auto max = [](auto &a, auto &b) { return a > b ? a : b; };
      for (int gg = 0; gg != nb_gauss_pts; ++gg) {

        t_strain(i, j) = (t_grad(i, j) || t_grad(j, i)) / 2.;

        double trace = t_strain(i, i);
        CHKERR commonData.strainSpecDecomp<
            FTensor::Tensor2_symmetric<FTensor::PackPtr<double *, 1>, 3>,
            FTensor::Tensor2_symmetric<double, 3>>(t_strain, strain_plus,
                                                   strain_minus);

        const double g_d = (commonData.*dgr_fun_ptr)(d);
        const double trace_plus = bracket_plus(trace);
        const double trace_min = bracket_minus(trace);

        const double energy_plus = 0.5 * lambda * trace_plus * trace_plus +
                             mu * strain_plus(i, j) * strain_plus(i, j);

        psi_plus = energy_plus;
        if (psi_plus < H)
          psi_plus = H;

        t_stress(0, 0) = lambda * trace_plus;
        t_stress(1, 1) = lambda * trace_plus;
        t_stress(2, 2) = lambda * trace_plus;
        t_stress(i, j) = t_stress(i, j) + 2 * mu * strain_plus(i, j);
        
        t_stress_plus(i, j) = t_stress(i, j) * commonData.diffDegrFunc(d); // it will be used in Lhs operators 
        
        t_stress(i, j) *= g_d;

        t_stress(0, 0) += lambda * trace_min;
        t_stress(1, 1) += lambda * trace_min;
        t_stress(2, 2) += lambda * trace_min;
        t_stress(i, j) = t_stress(i, j) + 2 * mu * strain_minus(i, j);

        ++t_stress;
        ++t_stress_plus;
        ++t_grad;
        ++t_strain;

        ++psi_plus;
        ++d;
        ++H;
      }

      MoFEMFunctionReturn(0);
    }
  };
  /**
   * @brief update history
   *
   * \f[
   * \mathcal{H}(t) = \underset{t}{\mathrm{max}} \, \psi^d(t)
   * \f]
   *
   */
  struct OpUpdateHistory
      : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

    MoFEM::Interface &mField;
    CommonData &commonData;
    BlockData &dAta;
    bool uPdate;
    OpUpdateHistory(MoFEM::Interface &m_field, string field_name,
                    CommonData &common_data, BlockData &data,
                    bool update = false)
        : MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
              field_name, UserDataOperator::OPROW),
          mField(m_field), commonData(common_data), dAta(data), uPdate(update) {
      doEdges = false;
      doQuads = false;
      doTris = false;
      doTets = false;
      doPrisms = false;
    }

    MoFEMErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;

      int nb_dofs = data.getFieldData().size();
      if (nb_dofs == 0)
        MoFEMFunctionReturnHot(0);

      if (dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) ==
          dAta.tEts.end()) {
        MoFEMFunctionReturnHot(0);
      }
      CHKERR commonData.getBlockData(dAta);

      EntityHandle fe_ent = getFEEntityHandle();
      int nb_gauss_pts = data.getN().size1();
      const int nb_base_functions = data.getN().size2();
      auto t_grad = getFTensor2FromMat<3,3>(*commonData.gradDispPtr);

      CHKERR commonData.getHistoryVar(fe_ent, nb_gauss_pts);
      auto H = getFTensor0FromVec(*commonData.historyVariablePtr);

      const double lambda = commonData.lAmbda;
      const double mu = commonData.mU;

      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;
      FTensor::Tensor2<double, 3, 3> strainT;
      FTensor::Tensor2_symmetric<double, 3> strain_plus;
      FTensor::Tensor2_symmetric<double, 3> strain_minus;

      // Macaulay bracket
      auto bracket_plus = [](auto &a) { return a > 0. ? a : 0; };
      auto max = [](auto a, auto b) { return a > b ? a : b; };

      for (int gg = 0; gg != nb_gauss_pts; ++gg) {

        strainT(i, j) = (t_grad(i, j) + t_grad(j, i)) / 2;
        double trace = strainT(i, i);

        CHKERR commonData.strainSpecDecomp<FTensor::Tensor2<double, 3, 3>, 
                                FTensor::Tensor2_symmetric<double, 3>>(
            strainT, strain_plus, strain_minus);

        const double trace_plus = bracket_plus(trace);
        double energy_plus = 0.5 * lambda * trace_plus * trace_plus +
                             mu * strain_plus(i, j) * strain_plus(i, j);

        H = max(H, energy_plus);

        ++H;
        ++t_grad;
      }

      CHKERR commonData.setHistoryVar(fe_ent, nb_gauss_pts);

      MoFEMFunctionReturn(0);
    }
  };
  /**
   * @brief calculate f^init_d
   * \f[
   * f_d^{\mathrm{int}} = \int_\Omega \left[ \mathcal{G}_c \left( \frac{1}{2l_0}
   * N_d^T N_d + 2 l_0 \mathbf{B}_d^T \mathbf{B}_d \right) d + \frac{\partial
   * g}{ \partial d} \mathcal{H} N_d^T \right] \, \mathrm d V \f]
   */
  struct OpAssembleDRhs
      : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

    CommonData &commonData;
    VectorDouble nF; ///< Vector of the right hand side (internal forces)
    BlockData &dAta;
    OpAssembleDRhs(CommonData &common_data, BlockData &data)
        : VolumeElementForcesAndSourcesCore::UserDataOperator(
              "PHASE_FIELD", UserDataOperator::OPROW),
          commonData(common_data), dAta(data) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {

      MoFEMFunctionBegin;

      const int nb_dofs = data.getIndices().size();
      if (!nb_dofs)
        MoFEMFunctionReturnHot(0);

      if (dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) ==
          dAta.tEts.end()) {
        MoFEMFunctionReturnHot(0);
      }
      CHKERR commonData.getBlockData(dAta);

      nF.resize(nb_dofs, false);
      nF.clear();

      const int nb_gauss_pts = data.getN().size1();
      const int nb_base_functions = data.getN().size2();
      auto base_functions = data.getFTensor0N();
      auto diff_base_functions = data.getFTensor1DiffN<3>();
      if (!commonData.cPtr) {
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "cPtr is null");
      }
      auto d = getFTensor0FromVec(*commonData.cPtr);
      auto grad_d = getFTensor1FromMat<3>(*commonData.gradcPtr);
      auto psi_plus = getFTensor0FromVec(*commonData.psiPtr);
      const double gc = commonData.gC;
      const double l = commonData.l0;

      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;

      for (int gg = 0; gg != nb_gauss_pts; gg++) {

        // Get volume and integration weight
        double w = getVolume() * getGaussPts()(3, gg);
        if (getHoGaussPtsDetJac().size() > 0) {
          w *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
        }

        int bb = 0;
        for (; bb != nb_dofs; bb++) {

          nF[bb] += w * gc * d * (1. / (l)*base_functions);

          nF[bb] += w * gc * l * diff_base_functions(i) * grad_d(i);
          nF[bb] += w * commonData.diffDegrFunc(d) * psi_plus * base_functions;
          ++base_functions;
          ++diff_base_functions;
        }
        // Could be that we base more base functions than needed to approx.
        // phase field.
        for (; bb != nb_base_functions; bb++) {
          ++base_functions;
          ++diff_base_functions;
        }

        // Increment quantities for integration pts.
        ++grad_d;
        ++psi_plus;
        ++d;
      }

      // Assemble internal force vector for snes solver
      CHKERR VecSetValues(getFEMethod()->snes_f, nb_dofs,
                          &*data.getIndices().begin(), &*nF.begin(),
                          ADD_VALUES);

      MoFEMFunctionReturn(0);
    }
  };
  /**
   * @brief assemple f^init_u
   * \f[
   *      f_u^{\mathrm{int}} = \int_\Omega  \mathbf{B}_u^T \left( g \mathbf{D}^d
   * + \mathbf{D}^i \right) \mathbf{B}_u \mathbf u \,\mathrm d V =\int_\Omega
   * \mathbf{B}_u^T \mathbf \sigma   \,\mathrm d V \f]
   */
  struct OpAssembleURhs
      : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {

    CommonData &commonData;
    VectorDouble nF;
    BlockData &dAta;

    OpAssembleURhs(CommonData &common_data, BlockData &data)
        : VolumeElementForcesAndSourcesCore::UserDataOperator(
              "U", UserDataOperator::OPROW),
          commonData(common_data), dAta(data) {}

    MoFEMErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;

      int nb_dofs = data.getFieldData().size();
      if (nb_dofs == 0)
        MoFEMFunctionReturnHot(0);

      if (dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) ==
          dAta.tEts.end()) {
        MoFEMFunctionReturnHot(0);
      }
      CHKERR commonData.getBlockData(dAta);

      int nb_gauss_pts = data.getN().size1();
      int nb_base_functions = data.getN().size2();
      nF.resize(nb_dofs, false);
      nF.clear();
      auto diff_base_functions = data.getFTensor1DiffN<3>();
      auto t_stress = getFTensor2SymmetricFromMat<3>(*commonData.sTressPtr);

      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;

      for (int gg = 0; gg != nb_gauss_pts; gg++) {

        double val = getVolume() * getGaussPts()(3, gg);
        if (getHoGaussPtsDetJac().size() > 0) {
          val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
        }

        int bb = 0;
        for (; bb != nb_dofs / 3; bb++) {
          double *ptr = &nF[3 * bb];
          FTensor::Tensor1<double *, 3> t1(ptr, &ptr[1], &ptr[2]);
          t1(i) += val * t_stress(i, j) * diff_base_functions(j);
          ++diff_base_functions;
        }

        // Could be that we base more base functions than needed to approx.
        // disp. field.
        for (; bb != nb_base_functions; bb++) {
          ++diff_base_functions;
        }
        ++t_stress;
      }

      CHKERR VecSetValues(getFEMethod()->snes_f, nb_dofs, &data.getIndices()[0],
                          &nF[0], ADD_VALUES);

      MoFEMFunctionReturn(0);
    }
  };

  struct OpPostProcStress
      : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {
    CommonData &commonData;
    moab::Interface &postProcMesh;
    std::vector<EntityHandle> &mapGaussPts;
    BlockData &dAta;

    OpPostProcStress(moab::Interface &post_proc_mesh,
                     std::vector<EntityHandle> &map_gauss_pts,
                     CommonData &common_data, BlockData &data)
        : VolumeElementForcesAndSourcesCore::UserDataOperator(
              "U", UserDataOperator::OPROW),
          commonData(common_data), postProcMesh(post_proc_mesh),
          mapGaussPts(map_gauss_pts), dAta(data) {
      doVertices = true;
      doEdges = false;
      doQuads = false;
      doTris = false;
      doTets = false;
      doPrisms = false;
    }

    MoFEMErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;

      double def_VAL[9];
      bzero(def_VAL, 9 * sizeof(double));

      if (dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) ==
          dAta.tEts.end()) {
        MoFEMFunctionReturnHot(0);
      }
      CHKERR commonData.getBlockData(dAta);

      Tag th_stress;
      CHKERR postProcMesh.tag_get_handle("STRESS", 9, MB_TYPE_DOUBLE, th_stress,
                                         MB_TAG_CREAT | MB_TAG_SPARSE, def_VAL);
      Tag th_strain;
      CHKERR postProcMesh.tag_get_handle("STRAIN", 9, MB_TYPE_DOUBLE, th_strain,
                                         MB_TAG_CREAT | MB_TAG_SPARSE, def_VAL);
      Tag th_psi;
      CHKERR postProcMesh.tag_get_handle("ENERGY", 1, MB_TYPE_DOUBLE, th_psi,
                                         MB_TAG_CREAT | MB_TAG_SPARSE, def_VAL);

      auto t_grad = getFTensor2FromMat<3,3>(*commonData.gradDispPtr);
      auto d = getFTensor0FromVec(*commonData.cPtr);

      const int nb_gauss_pts = commonData.gradDispPtr->size2();
      const int nb_gauss_pts2 = commonData.cPtr->size();

      const double lambda = commonData.lAmbda;
      const double mu = commonData.mU;

      FTensor::Index<'i', 3> i;
      FTensor::Index<'j', 3> j;
      FTensor::Tensor2<double, 3, 3> strain;
      FTensor::Tensor2<double, 3, 3> strain_plus_non;
      FTensor::Tensor2<double, 3, 3> strain_minus_non;
      FTensor::Tensor2_symmetric<double, 3> strain_plus;
      FTensor::Tensor2_symmetric<double, 3> strain_minus;

      // Macaulay bracket
      auto bracket_plus = [](auto &a) { return a > 0. ? a : 0.; };
      auto bracket_minus = [](auto &a) { return a < 0. ? a : 0.; };
      for (int gg = 0; gg != nb_gauss_pts; gg++) {

        FTensor::Tensor2<double, 3, 3> t_stress(0., 0., 0., 0., 0., 0., 0., 0., 0.);
        strain(i, j) = (t_grad(i, j) + t_grad(j, i)) / 2.;

        double trace = strain(i, i);
        CHKERR commonData
            .strainSpecDecomp<FTensor::Tensor2<double, 3, 3>,
                              FTensor::Tensor2_symmetric<double, 3>>(
                strain, strain_plus, strain_minus);

        const double trace_plus = bracket_plus(trace);
        const double trace_min = bracket_minus(trace);
        const double energy_plus = 0.5 * lambda * trace_plus * trace_plus +
                             mu * strain_plus(i, j) * strain_plus(i, j);

        const double energy_minus = 0.5 * lambda * trace_min * trace_min +
                              mu * strain_minus(i, j) * strain_minus(i, j);
        const double g_d = commonData.degrFuncClr(d);
        const double psi = energy_plus * g_d + energy_minus;

        symm_to_non_tensor(strain_plus, strain_plus_non); //this is necessary for tag sizes (cannot be 6)
        symm_to_non_tensor(strain_minus, strain_minus_non);

        t_stress(0, 0) = lambda * trace_plus;
        t_stress(1, 1) = lambda * trace_plus;
        t_stress(2, 2) = lambda * trace_plus;
        t_stress(i, j) += 2 * mu * strain_plus_non(i, j);
        t_stress(i, j) *= g_d;

        t_stress(0, 0) += lambda * trace_min;
        t_stress(1, 1) += lambda * trace_min;
        t_stress(2, 2) += lambda * trace_min;
        t_stress(i, j) += 2 * mu * strain_minus_non(i, j);

        CHKERR postProcMesh.tag_set_data(th_psi, &mapGaussPts[gg], 1, &psi);
        CHKERR postProcMesh.tag_set_data(th_strain, &mapGaussPts[gg], 1,
                                         &strain(0, 0));
        CHKERR postProcMesh.tag_set_data(th_stress, &mapGaussPts[gg], 1,
                                         &t_stress(0, 0));
        ++d;
        ++t_grad;
      }

      MoFEMFunctionReturn(0);
    }
  };


};


#endif //__PHASEFIELDHPP__
